# README #
### Translation ###
0.creating
xgettext -d base -o locale/teacher.pot teacher.py
1. updating
msgmerge --update locale/uz_UZ/LC_MESSAGES/teacher.po locale/teacher.pot
msgmerge --update locale/ru_RU/LC_MESSAGES/teacher.po locale/teacher.pot
msgmerge --update locale/en_US/LC_MESSAGES/teacher.po locale/teacher.pot
2. building mo
msgfmt -o locale/uz_UZ/LC_MESSAGES/teacher.mo locale/uz_UZ/LC_MESSAGES/teacher.po
msgfmt -o locale/ru_RU/LC_MESSAGES/teacher.mo locale/ru_RU/LC_MESSAGES/teacher.po
msgfmt -o locale/en_US/LC_MESSAGES/teacher.mo locale/en_US/LC_MESSAGES/teacher.po

### What is this repository for? ###

* This bot is done for teachers and pupils. It is simple test that pupil can take, see results. On the other hand teachers
can insert tests, cotnrol his/her classes, their marks
* 0.0.1 not stable

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
To run application create config.json with following:
    {
      "token": "MY_TELEGRAM_TOKEN_GIVEN_BY_BOT_FATHER",
      "db": "db/DB_NAME.sqlite"
    }
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact