#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Basic example for a bot that uses inline keyboards.
"""
import hashlib
import logging
import os
import sys
import time
import traceback

from telegram import ReplyKeyboardMarkup, KeyboardButton, ParseMode
from telegram.bot import Bot
from telegram.ext import CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram.utils.helpers import mention_html

from db_manager import DBManager
from files.read_docx import read_answers, read_question_docx
from general_functions import *
from learner import send_student_test_analyze

_ru = None
_uz = None
_us = None
token = None
db = None
bot: Bot = None


def start(update, context):
    global db, bot
    try:
        chat_id = update.effective_message.chat.id
        if db.if_teacher_exist(chat_id):
            send_main_menu(update, context)
            return
        send_languages(chat_id, bot)
    except Exception as ex:
        logger.error(ex)


def handle_inline_keyboard(update, context):
    global token, db, bot
    try:
        query = update.callback_query
        data = query.data
        data = json.loads(data)
        # key, value = data.split(":")
        print("data: ", data)
        chat_id = update.effective_chat.id
        if data["key"] == KEY_LANGUAGE:  # "language":
            user_locale = context.user_data['locale'] = data['locale']
            reply_markup = ReplyKeyboardMarkup([[KeyboardButton(button(user_locale, BUTTON_REGISTRATION))]],
                                               one_time_keyboard=True, resize_keyboard=True)
            bot.send_message(chat_id=chat_id,
                             text=tr(user_locale,
                                     _("Bu dastur turli sohadagi ta'lim beruvchilar uchun tuzilgan bo'lib, unda "
                                       "*ta'lim oluvchining bilimini tekshirish jarayoni avtomatlashtirilgan*. "
                                       " Hozircha bilimni tekshirish test asosida olib boriladi. Keyinchalik esa "
                                       " ta'lim beruvchilar talabiga binoan o'zgartirishlar, qo'shimcha imkoniyatlar "
                                       " kiritiladi."
                                       "\n")) +
                                  tr(user_locale,
                                     _("Dasturdan foydalanish uchun *{button}* tugmasini bosib ro'yxatdan o'ting")).
                             format(button=button(user_locale, BUTTON_REGISTRATION)),
                             reply_markup=reply_markup,
                             parse_mode=ParseMode.MARKDOWN)
        elif data["key"] == KEY_TEST_GROUP:  # "test_group":
            # test_group_id, action = value.split("-")
            # action = int(action)
            context.user_data['test_group_id'] = data["id"]
            context.user_data['test_group_name'] = data["name"]
            menu = data["menu"]
            # if menu == BUTTON_EXAM_MONITOR:
            #     bot.send_message(chat_id=chat_id, text=_("Test jarayonini kuzatish"))
            if menu == ACTION_EXAM_CREATE:
                create_exam(chat_id, context, context.user_data['group_name'], data['id'])
            elif menu == BUTTON_TEST_VIEW:
                send_test_content(update,context, data["id"])
            elif menu == BUTTON_TEST_INSERT:
                if not data["id"]:
                    user_locale = get_user_locale(chat_id, context)
                    context.user_data['test_group_id'], context.user_data['test_group_name'] = db.insert_test_group(
                        chat_id)
                    if not context.user_data['test_group_id']:
                        bot.send_message(chat_id=chat_id,
                                         text=tr(user_locale, _("Xatolik yuz berdi")))
                        return
                context.user_data['inline'] = {'key': "test_topic"}
                handle_test_add(update, context)
            elif menu == BUTTON_TEST_DELETE:
                # test_group_name = context.user_data['test_group_name']
                send_test_list(update, context)
            elif menu == BUTTON_TEST_INSERT_FILE:
                # context.user_data['theme_id'] = theme_id
                if not data["id"]:
                    user_locale = get_user_locale(chat_id, context)
                    context.user_data['test_group_id'], context.user_data['test_group_name'] = db.insert_test_group(
                        chat_id)
                    if not context.user_data['test_group_id']:
                        bot.send_message(chat_id=chat_id,
                                         text=tr(user_locale, _("Xatolik yuz berdi")))
                        return
                context.user_data['doc'] = DOC_INIT
                handle_document(update, context)
            elif menu == BUTTON_TEST_GROUP_DELETE:
                delete_tests(update, context, test_group_id=data['id'])
        elif data["key"] == KEY_TEST:  # "test":
            # test_id, action = value.split("-")
            # test_id, action = int(test_id), int(action)
            test_id = data["test_id"]
            menu = data["menu"]
            # yangi KEY_TEST_GROUP_DELETE ochib , pastdagi hammasini o'chirish ga tegishli kodni ko'chirish
            if menu == BUTTON_TEST_DELETE:
                delete_tests(update, context, test_id=test_id)
        elif data["key"] == KEY_NEW_TEST:  # "new_test":
            inline_data = context.user_data['inline']
            test_group_id = context.user_data['test_group_id']
            if db.save_test(test_group_id=test_group_id, question=inline_data['question'],
                            answers=inline_data['answers'], right_answer=data["answer"]):
                button_list = []
                context.user_data['inline'] = {}
                context.user_data['inline']['key'] = "test_topic"
                # callback_data = 'test_group:{}-{}'.format(test_group_id, BUTTON_TEST_INSERT)
                callback_data = json.dumps({"key": KEY_TEST_GROUP, "name": context.user_data['test_group_name'],
                                            "id": test_group_id, "menu": BUTTON_TEST_INSERT})
                button_list.append([InlineKeyboardButton("Yana kiritish", callback_data=callback_data)])
                reply_markup = InlineKeyboardMarkup(button_list)
                bot.send_message(chat_id=chat_id,
                                 text="Test muvaffaqiyatli saqlandi.",
                                 reply_markup=reply_markup)
            else:
                bot.send_message(chat_id=chat_id,
                                 text="Test saqlashda xatolik bo'ldi")
        elif data["key"] == KEY_EXAM_MONITOR:
            message_id = update.effective_message.message_id
            if data["action"] == ACTION_EMPTY:
                send_exam_list(chat_id, context, message_id, data['key'])
            elif data['action'] == ACTION_MONITOR:
                status = db.get_exam_status(data['id'])
                send_exam_result_info(update, context, data['id'])
                send_monitor_menu(update, context, status)
                monitor_start(update, context, data['id'])
            elif data['action'] == ACTION_BACK_TO_MONITOR:
                monitor_start(update, context, data['id'])
            elif data['action'] == ACTION_EXAM_ANALYZE:
                send_student_list(chat_id, context, message_id, data)
            elif data['action'] == ACTION_EXAM_ANALYZE_STUDENT:
                callback_data = json.dumps({"key": data['key'], "id": data['id'],"exam_id": data['id'],
                                            "action": ACTION_BACK_TO_EXAM_ANALYZE})
                reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton("Orqaga", callback_data=callback_data)]])
                callback = lambda t: bot.send_message(chat_id=chat_id,
                                                      text=t,
                                                      parse_mode=ParseMode.MARKDOWN,
                                                      reply_markup=reply_markup)
                send_student_test_analyze(db, context, chat_id, data['id'], callback, query['id'], data['student_id'])
            elif data['action'] == ACTION_BACK_TO_EXAM_ANALYZE:
                send_student_list(chat_id, context, message_id, data)
        elif data["key"] == KEY_EXAM_SEND:
            message_id = update.effective_message.message_id
            if data["action"] == ACTION_EMPTY:
                send_exam_list(chat_id, context, message_id, data['key'])
            elif data["action"] == ACTION_EXAM_CREATE:
                # create_exam(chat_id, data['name'], data['id'])
                context.user_data['menu'] = ACTION_EXAM_CREATE
                user_locale = get_user_locale(chat_id, context)
                rows = db.get_test_group(chat_id)
                if len(rows):
                    text = tr(user_locale,
                              _("Endi imtihon nomini kiriting. Nom ma'noli va hamma imtihondan farqli "
                                "bo'lsa, ularni bir-biridan ajratishingiz oson bo'ladi"))
                else:
                    text = tr(user_locale, _("Sizda test to'plami mavjud emas. Avval uni kiriting"))
                bot.edit_message_text(chat_id=chat_id, message_id=message_id, text=text)
            elif data["action"] == ACTION_EXAM_SEND_LINK:
                send_exam_link(chat_id, context, message_id, exam_id=data['id'])

    except Exception as error:
        logger.error(error)


def send_monitor_menu(update: Updater, context, status):
    chat_id = update.effective_chat.id
    user_locale = get_user_locale(chat_id, context)
    reply_markup = ReplyKeyboardMarkup([[KeyboardButton(button(user_locale, BUTTON_MONITOR_BACK)),
                                         KeyboardButton(button(user_locale, BUTTON_MONITOR_PLAY))
                                         if status != EXAM_STATUS_BEGAN
                                         else KeyboardButton(button(user_locale, BUTTON_MONITOR_STOP))
                                         ]],
                                       one_time_keyboard=True, resize_keyboard=True)
    if status != EXAM_STATUS_BEGAN:
        text = tr(user_locale, _("Imtihonni boshlash uchun *{}* tugmasini bosing.")). \
            format(button(user_locale, BUTTON_MONITOR_PLAY))
    else:
        text = tr(user_locale, _("Imtihonni tugatish uchun *{}* tugmasini bosing.")). \
            format(button(user_locale, BUTTON_MONITOR_STOP))
    bot.send_message(chat_id=chat_id,
                     text=text,
                     reply_markup=reply_markup,
                     parse_mode=ParseMode.MARKDOWN)


def handle_text(update: Updater, context):
    global db, bot
    chat_id = update.effective_chat.id
    user_locale = get_user_locale(chat_id, context)
    if not user_locale:
        bot.send_message(chat_id=chat_id, text="/start")
        return
    message = update.message.text
    if message == button(user_locale, BUTTON_REGISTRATION):
        context.user_data['menu'] = BUTTON_REGISTRATION
        context.user_data['value'] = {'first_name': None, "last_name": None, "field": None}
        bot.send_message(chat_id=chat_id, text=tr(user_locale, _("Ismingizni kiriting")))
    elif message == button(user_locale, BUTTON_EXAM):
        # https://t.me/triviabot?startgroup=test
        send_exam_menu(update, context)
    elif message == button(user_locale, BUTTON_TEST):
        send_test_menu(update, context)
        context.user_data['menu'] = BUTTON_TEST
    elif message == button(user_locale, BUTTON_BACK):
        send_main_menu(update, context)
    elif message == button(user_locale, BUTTON_TEST_VIEW):
        send_test_group(update, context, BUTTON_TEST_VIEW)
    elif message == button(user_locale, BUTTON_TEST_DELETE):
        send_test_group(update, context, BUTTON_TEST_DELETE)
    elif message == button(user_locale, BUTTON_TEST_INSERT):
        send_test_group(update, context, BUTTON_TEST_INSERT)
    elif message == button(user_locale, BUTTON_TEST_INSERT_FILE):
        send_test_group(update, context, BUTTON_TEST_INSERT_FILE)
    elif message == button(user_locale, BUTTON_MONITOR_BACK):
        monitor_stop(context)
        send_main_menu(update, context)
    elif message == button(user_locale, BUTTON_MONITOR_STOP):
        send_message_to_all(context.bot_data['exam_id'], tr(user_locale, _("Imtihon tugadi. Endi test tahlilini ko'rishingiz mumkin")))
        db.set_exam_state(context.bot_data['exam_id'], EXAM_STATUS_FINISHED)
        send_monitor_menu(update, context, False)
    elif message == button(user_locale, BUTTON_MONITOR_PLAY):
        send_message_to_all(context.bot_data['exam_id'], tr(user_locale, _("Imtihon boshlandi. Diqqat! Imtihon tugamaguncha test tahlilidan foydalana olmaysiz")))
        db.set_exam_state(context.bot_data['exam_id'], EXAM_STATUS_BEGAN)
        send_monitor_menu(update, context, True)
    else:  # agar yozuv bo'lsa
        menu = context.user_data['menu'] if 'menu' in context.user_data else None
        if menu == BUTTON_REGISTRATION:
            if not context.user_data['value']['first_name']:
                context.user_data['value']['first_name'] = message
                bot.send_message(chat_id=chat_id, text=tr(user_locale, _("Familiyangizni kiriting")))
            elif not context.user_data['value']['last_name']:
                context.user_data['value']['last_name'] = message
                bot.send_message(chat_id=chat_id, text=tr(user_locale,
                                                          _("Kasbingiz")))
            elif not context.user_data['value']['field']:
                if 'locale' not in context.user_data:
                    bot.send_message(chat_id=chat_id, text="/start")
                context.user_data['value']['field'] = message
                if db.register_teacher(chat_id, context.user_data['locale'], context.user_data['value']):
                    del context.user_data['value']
                    del context.user_data['menu']
                    bot.send_message(chat_id=chat_id, text=ICON_TADA + tr(user_locale,
                                                                          _(
                                                                              " Siz ro'yxatdan muvaffaqiyatli o'tdingiz")))
                    send_main_menu(update, context)
                else:
                    bot.send_message(chat_id=chat_id, text=ICON_WRONG + tr(user_locale,
                                                                           _(" Xatolik yuz berdi")))
        elif menu == BUTTON_TEST_INSERT and 'inline' in context.user_data:
            handle_test_add(update, context)
        elif menu == ACTION_EXAM_CREATE:
            context.user_data['group_name'] = message
            send_test_group(update, context, menu)


def send_message_to_all(exam_id, message):
    global db
    rows = db.get_students(exam_id)

    with open('config.json', 'r') as file:
        config = json.load(file)
        token = config['student_token']
        bot = Bot(token=token)
    for row in rows:
        bot.send_message(chat_id=row['chat_id'], text=message)


def send_exam_link(chat_id, context, message_id, exam_id=None, hash_value=None):
    global db
    if not hash_value:
        hash_value = hashlib.md5(str(exam_id).encode())
        hash_value = hash_value.hexdigest()
    row = db.get_link_param(chat_id, hash_value)
    user_locale = get_user_locale(chat_id, context)

    with open('config.json', 'r') as bot_file:
        config_file = json.load(bot_file)
        bot_name = config_file['teacher_name']

    logger.info("reading database")

    text = tr(user_locale, _("Sizni {} \"{}\" nomli imtihonga taklif qilmoqda\n"
                             "t.me/{}?start={}").format(row['teacher'], row['exam'],
                                                        bot_name, hash_value))
    if message_id:
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text=text)
    else:
        bot.send_message(chat_id=chat_id,
                         text=text)


def create_exam(chat_id, context, exam_name, test_group_id):
    last_id = db.insert_exam(exam_name, test_group_id, chat_id)

    if not last_id:
        send_error(chat_id, context, ("error: last_id is None",))
        return
    hash_value = hashlib.md5(str(last_id).encode())
    if not hash_value:
        send_error(chat_id, context, ("error: hash_vlaue is None",))
        return
    hash_value = hash_value.hexdigest()
    if not hash_value or not db.save_hash_value(last_id, hash_value):
        send_error(chat_id, context, ('error in saving hash value: ', hash_value,))
        return
    send_exam_link(chat_id, context, None, hash_value=hash_value)


def send_error(chat_id, context, args):
    user_locale = get_user_locale(chat_id, context)
    bot.send_message(chat_id=chat_id, text=ICON_WRONG + tr(user_locale, _(" Xatolik yuz berdi")))
    print(args)


def delete_tests(update: Updater, context, test_id=None, test_group_id=None):
    # test_group_id = context.user_data['test_group_id']
    chat_id = update.effective_chat.id
    # test_group_name = context.user_data['test_group_name']
    # agar test == 0, u holda hammasini o'chir
    user_locale = get_user_locale(chat_id, context)
    if not test_id and not test_group_id:
        logger.error("test_id and group_id are None")
        return

    if db.is_test_used(test_id, test_group_id):
        bot.send_message(chat_id=chat_id,
                         text=tr(user_locale, _("Test o'quvchilar tarafidan imtihonda ishlatilganligi uchun "
                                                "uni o'chirish mumkin emas.")))
    elif db.delete_test(test_id, test_group_id):
        # bot.send_message(chat_id=chat_id,
        #                  text=_("Test muvaffaqiyatli o'chirildi."))
        if test_id:
            send_test_list(update, context, edit_mode=True)
        else:
            send_test_group(update, context, BUTTON_TEST_DELETE, edit_mode=True)
    else:
        bot.send_message(chat_id=chat_id,
                         text=tr(user_locale, _("Testni o'chirishda xatolik bo'ldi")))


def handle_test_add(update: Updater, context):
    if not isinstance(context.user_data, dict) or (isinstance(context.user_data, dict) and
                                                   "inline" not in context.user_data.keys()):
        logger.error("Error in context.user_data")
        return
    key = context.user_data['inline']['key']
    text = ""

    if key == "test_topic":
        # context.user_data['inline']['position'] += 1
        text = """Savolni kiriting"""
        # context.user_data['inline']['topic'] = update.message.text
        context.user_data['inline']['key'] = "test_question"
    elif key == "test_question":
        text = "Javobni kiriting. Javoblar kamida 2 ta, ko'pida 5 ta bo'lishi mumkin. \n Javoblarni " \
               "kiritish tugagach, to'g'ri javobni belgilang"
        context.user_data['inline']['question'] = update.message.text
        context.user_data['inline']['key'] = "test_answer"
    elif key == "test_answer":
        if 'answers' not in context.user_data['inline']:
            context.user_data['inline']['answers'] = []
        if len(context.user_data['inline']['answers']) >= 5:
            text = "5 tadan ortiq javob kiritish mumkin emas"
        else:
            text = "Navbatdagi javobni kiriting."
            context.user_data['inline']['answers'].append(update.message.text)
            if len(context.user_data['inline']['answers']) > 1:
                text = "Hohlasangiz yana javob kiritishingiz mumkin. Aks holda, javobni tanlang."
                send_created_test(context.user_data['inline']['question'],
                                  context.user_data['inline']['answers'], update)
            if len(context.user_data['inline']['answers']) == 5:
                text = "To'g'ri javobni tanlang"

    bot.send_message(chat_id=update.effective_chat.id,
                     text=text,
                     parse_mode=ParseMode.MARKDOWN)


def send_created_test(question: str, answers: [], update: Updater):
    global bot
    button_list = []
    i = 1
    for answer in answers:
        if answer:
            callback_data = json.dumps({"key": KEY_NEW_TEST, "answer": format(i)})
            # button_list.append([InlineKeyboardButton(answer, callback_data='new_test:{}'.format(i))])
            button_list.append([InlineKeyboardButton(answer, callback_data=callback_data)])
            i += 1
    reply_markup = InlineKeyboardMarkup(button_list)
    bot.send_message(chat_id=update.effective_chat.id, text=question, reply_markup=reply_markup)


def send_test_content(update, context, test_group_id):
    chat_id = update.effective_chat.id
    rows = db.get_tests(test_group_id)
    user_locale = get_user_locale(chat_id, context)
    if len(rows) == 0:
        bot.send_message(chat_id=chat_id,
                         text=tr(user_locale, _("Bu imtihonda testlar mavjud emas")))
        return

    try:
        result = ''
        i = 0
        get_answer = lambda answer, right, ind: '\n{} {}'. \
            format((ICON_CHECK if int(right) == ind else ''), answer) if answer else ''
        for row in rows:
            i += 1

            result += tr(user_locale, ("\n\n*Savol {}* {}\nJavoblar:{}{}{}{}{}")).format(
                i,
                row['question'],
                get_answer(row['answer1'], row['right_answer'], 1),
                get_answer(row['answer2'], row['right_answer'], 2),
                get_answer(row['answer3'], row['right_answer'], 3),
                get_answer(row['answer4'], row['right_answer'], 4),
                get_answer(row['answer5'], row['right_answer'], 5)
            )

        bot.send_message(chat_id=chat_id,
                         text=result,
                         parse_mode=ParseMode.MARKDOWN)
    except Exception as ex:
        logger.error(ex)


def send_test_list(update, context, edit_mode=False):
    test_group_id = context.user_data['test_group_id']
    test_group_name = context.user_data['test_group_name']
    rows = db.get_tests(test_group_id)
    chat_id = update.effective_chat.id
    user_locale = get_user_locale(chat_id, context)
    button_list = []
    if len(rows) == 0:
        send_message(chat_id, tr(user_locale, _("Bu imtihonda testlar mavjud emas")))
        return
    i = 0
    question = tr(user_locale, _("Savol"))
    for row in rows:
        i += 1
        callback_data = json.dumps({"key": KEY_TEST, "test_id": row['id'], "menu": BUTTON_TEST_DELETE})
        button_list.append([InlineKeyboardButton("{} {}: {}".format(question, i, row['question']),
                                                 # callback_data='test:{}-{}'.format(row['id'], action))])
                                                 callback_data=callback_data)])

    # if action == BUTTON_TEST_DELETE:
    text = tr(user_locale,
              _("*{}* test to'plamiga tegishli qaysi testni o'chirmoqchisiz? Bittasini tanlang ".format(test_group_name)))
    # callback_data = json.dumps({"key": KEY_TEST, "test_id": 0, "action": action})
    # button_list.append([InlineKeyboardButton(_("Hammasini o'chirish"), callback_data=callback_data)])

    reply_markup = InlineKeyboardMarkup(button_list)
    if edit_mode:
        bot.edit_message_text(chat_id=chat_id,
                              message_id=update.effective_message.message_id,
                              text=tr(user_locale, _("Test muvaffaqiyatli o'chirildi.")),
                              reply_markup=reply_markup)
    else:
        bot.send_message(chat_id=update.effective_chat.id, text=text + ICON_BELOW,
                         reply_markup=reply_markup, parse_mode=ParseMode.MARKDOWN)


def send_not_available(chat_id):
    bot.send_message(chat_id=chat_id, text=_("Bu funktsiya xali ishga tushmadi"), parse_mode=ParseMode.MARKDOWN)


def send_message(chat_id, text):
    bot.send_message(chat_id=chat_id, text=text, parse_mode=ParseMode.MARKDOWN)


def send_exam_menu(update, context):
    global db, bot
    chat_id = update.effective_chat.id
    user_locale = get_user_locale(chat_id, context)
    button_list = [[InlineKeyboardButton(tr(user_locale, _("Imtihonni kuzatish")), callback_data=json.dumps(
        {"key": KEY_EXAM_MONITOR, "action": ACTION_EMPTY}))],
                   [InlineKeyboardButton(tr(user_locale, _("Imtihonga taklif qilish")), callback_data=json.dumps(
                       {"key": KEY_EXAM_SEND, "action": ACTION_EMPTY}))]]

    reply_markup = InlineKeyboardMarkup(button_list)
    bot.send_message(chat_id=chat_id, text=tr(user_locale, _("Amallardan birini tanlang ")) + ICON_BELOW,
                     reply_markup=reply_markup)


def send_student_list(chat_id, context, message_id, data):
    global db, bot
    key, exam_id = data['key'], data['exam_id']
    rows = db.get_student_list(exam_id)
    user_locale = get_user_locale(chat_id, context)
    button_list = []
    for row in rows:
        callback_data = json.dumps({"key": key, "id": exam_id, "student_id": row["id"],
                                    "action": ACTION_EXAM_ANALYZE_STUDENT})

        button_list.append([InlineKeyboardButton(row['name'], callback_data=callback_data)])

    callback_data = json.dumps({"key": key, "id": exam_id,
                                "action": ACTION_BACK_TO_MONITOR})

    button_list.append([InlineKeyboardButton("Orqaga", callback_data=callback_data)])
    reply_markup = InlineKeyboardMarkup(button_list)

    try:
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text="Tahlilni ko'rish uchun o'quvchilardan birini tanlang" + ICON_BELOW,
                              reply_markup=reply_markup, parse_mode=ParseMode.MARKDOWN)
    except Exception as ex:
        logger.log(ex)

def send_exam_list(chat_id, context, message_id, key):
    global db, bot
    rows = db.get_exam_list(chat_id)
    user_locale = get_user_locale(chat_id, context)
    button_list = []

    for row in rows:
        callback_data = json.dumps({"key": key, "id": row["id"],
                                    "action": ACTION_EXAM_SEND_LINK if key == KEY_EXAM_SEND else ACTION_MONITOR})

        button_list.append([InlineKeyboardButton(row['name'], callback_data=callback_data)])

    if key == KEY_EXAM_SEND:
        callback_data = json.dumps({"key": key, "id": None, "action": ACTION_EXAM_CREATE})
        button_list.append(
            [InlineKeyboardButton(tr(user_locale, _("Yangi tuzish")), callback_data=callback_data)]
        )
        if len(rows):
            text = tr(user_locale, _("Qaysi imtihon havolasini yubormoqchisiz? Bittasini tanlang"))
        else:
            text = tr(user_locale, _("Sizda imtihonlar mavjud emas. Imtihon tuzish uchun *{}* tugmasini bosing")) \
                .format(tr(user_locale, _("Yangi tuzish")))
    else:
        if len(rows):
            text = tr(user_locale, _("Qaysi imtihonni kuzatmoqchisiz? Bittasini tanlang"))
        else:
            text = tr(user_locale, _("Sizda imtihonlar mavjud emas. Imtihon tuzish uchun quyidagi tugmalarni "
                                     "ketma-ketlikda bosing:\n1. *{}*\n2. *{}*\n3. *{}*"))\
                .format(button(user_locale, BUTTON_EXAM),
                        tr(user_locale, _("Imtihonga taklif qilish")),
                        tr(user_locale, _("Yangi tuzish")))

    reply_markup = InlineKeyboardMarkup(button_list)

    try:
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text=text + ICON_BELOW, reply_markup=reply_markup, parse_mode=ParseMode.MARKDOWN)
    except Exception as ex:
        logger.log(ex)


def send_test_group(update, context, menu, edit_mode=False):
    global db, bot
    chat_id = update.effective_chat.id
    rows = db.get_test_group(chat_id)
    context.user_data['menu'] = menu
    user_locale = get_user_locale(chat_id, context)
    if not rows and menu in [BUTTON_TEST_DELETE, BUTTON_TEST_VIEW]:
        send_message(chat_id, ICON_WARNING + tr(user_locale, _(" Sizda test to'plami mavjud emas")))
        return

    button_list = []
    user_locale = get_user_locale(chat_id, context)

    for row in rows:
        callback_data = json.dumps({"key": KEY_TEST_GROUP, "name": row["name"], "id": row["id"], "menu": menu})
        callback_data2 = json.dumps(
            {"key": KEY_TEST_GROUP, "name": row["name"], "id": row["id"], "menu": BUTTON_TEST_GROUP_DELETE})
        buttons = [InlineKeyboardButton(row['name'], callback_data=callback_data)]
        if menu == BUTTON_TEST_DELETE:
            buttons.append(InlineKeyboardButton(button(user_locale, BUTTON_TEST_DELETE), callback_data=callback_data2))
        button_list.append(buttons)

    if menu in [BUTTON_TEST_INSERT_FILE, BUTTON_TEST_INSERT]:
        callback_data = json.dumps({"key": KEY_TEST_GROUP, "name": None, "id": None, "menu": menu})
        button_list.append(
            [InlineKeyboardButton(tr(user_locale, _("Yangi tuzish")), callback_data=callback_data)]
        )
    reply_markup = InlineKeyboardMarkup(button_list)

    if menu == BUTTON_TEST_VIEW:
        text = tr(user_locale, _("Ko'rish uchun testlar to'plamini tanlang"))
    elif menu in [BUTTON_TEST_INSERT_FILE, BUTTON_TEST_INSERT]:
        if len(rows):
            text = tr(user_locale, _("Testni qaysi test to'plamiga qo'shasiz? Bittasini tanlang"))
        else:
            text = tr(user_locale, _("Sizda test to'plami mavjud emas. Test to'plamini tuzish uchun *{}* "
                                     "tugmasini bosing")).format(tr(user_locale, _("Yangi tuzish")))
    elif menu == BUTTON_TEST_DELETE:
        text = tr(user_locale, _("O'chirish uchun testlar to'plamini tanlang"))
    elif menu == ACTION_EXAM_CREATE:
        if len(rows):
            text = tr(user_locale, _("Imtihonni qaysi test to'plami asosida o'tkazmoqchisiz? Bittasini tanlang"))
        else:
            text = tr(user_locale, _("Avval test to'plamini tuzib oling. Buni *{}* bo'limida amalga oshirasiz"))\
                .format(button(user_locale, BUTTON_TEST))
            bot.send_message(chat_id=chat_id, text=text, parse_mode=ParseMode.MARKDOWN)
            return

    if edit_mode:
        bot.edit_message_text(chat_id=chat_id,
                              message_id=update.effective_message.message_id,
                              text=text + ICON_BELOW,
                              reply_markup=reply_markup)
    else:
        bot.send_message(chat_id=chat_id, text=text + ICON_BELOW, reply_markup=reply_markup,
                         parse_mode=ParseMode.MARKDOWN)


def send_test_menu(update: Updater, context):
    chat_id = update.effective_chat.id
    user_locale = get_user_locale(chat_id, context)
    button_list = [[KeyboardButton(text=button(user_locale, BUTTON_TEST_INSERT)),
                    KeyboardButton(text=button(user_locale, BUTTON_TEST_VIEW))],
                   [KeyboardButton(text=button(user_locale, BUTTON_TEST_INSERT_FILE)),
                    KeyboardButton(text=button(user_locale, BUTTON_TEST_DELETE))],
                   [KeyboardButton(text=button(user_locale, BUTTON_BACK))]]
    text = tr(user_locale,
              _("Test sahifasiga hush kelibsiz.\nTestlarni bittalab kiritish uchun *{}* tugmasini " \
                "bosing\nTestlaringiz docx formatli faylda bo'lsa, *{}* tugmasini bosib, avval testlar yozilgan faylni, " \
                "keyin alohida javoblar yozilgan faylni yuboring.")) \
        .format(button(user_locale, BUTTON_TEST_INSERT), button(user_locale, BUTTON_TEST_INSERT_FILE))

    reply_markup = ReplyKeyboardMarkup(button_list, resize_keyboard=True)
    bot.send_message(chat_id=update.effective_chat.id,
                     text=text,
                     reply_markup=reply_markup,
                     parse_mode=ParseMode.MARKDOWN)


def send_main_menu(update, context):
    chat_id = update.effective_chat.id
    user_locale = get_user_locale(chat_id, context)
    reply_markup = ReplyKeyboardMarkup([[KeyboardButton(button(user_locale, BUTTON_EXAM)),
                                         KeyboardButton(button(user_locale, BUTTON_TEST))
                                         ]],
                                       one_time_keyboard=True, resize_keyboard=True)
    bot.send_message(chat_id=chat_id,
                     text=tr(user_locale, _("Imtihon jarayonini ko'rmoqchi bo'lsangiz *{}* tugmasini bosing."
                                            "\nTestlar bilan ishlash uchun *{}* tugmasini bosing")).
                     format(button(user_locale, BUTTON_EXAM), button(user_locale, BUTTON_TEST)),
                     reply_markup=reply_markup,
                     parse_mode=ParseMode.MARKDOWN)


def handle_document(update: Updater, context):
    global bot

    if 'doc' not in context.user_data:
        return
    
    chat_id = update.effective_message.chat.id
    user_locale = get_user_locale(chat_id, context)
    cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/files/'
    question_file = '{}{}_question.docx'.format(cur_dir, chat_id)
    answer_file = '{}{}_answer.docx'.format(cur_dir, chat_id)

    if context.user_data['doc'] == DOC_INIT:
        send_message(chat_id, tr(user_locale, _("Testlarni fayl ko'rinishda yuborish ikki qadamdan iborat:\n  "
                                                "1. Testlar yozilgan faylni yuborish\n  "
                                                "2. Test javoblari yozilgan faylni yuborish.")))
        send_message(chat_id, tr(user_locale, _("1. Testlar yozilgan faylni yuboring")))
        context.user_data['doc'] = DOC_QUESTION
    elif context.user_data['doc'] == DOC_QUESTION:
        if update.message.document.file_name.endswith('docx'):
            doc_file = bot.getFile(update.message.document.file_id)
            doc_file.download(question_file)
            send_message(chat_id, tr(user_locale, _("2. Endi test javoblari yozilgan faylni yuboring")))
            context.user_data['doc'] = DOC_ANSWERS
        else:
            send_message(chat_id, ICON_WARNING + tr(user_locale, _(" Faqat docx formatli fayl qabul qilinadi")))
    elif context.user_data['doc'] == DOC_ANSWERS:
        if update.message.document.file_name.endswith('docx'):
            doc_file = bot.getFile(update.message.document.file_id)
            doc_file.download(answer_file)
            context.user_data['doc'] = DOC_FINISH
        else:
            send_message(chat_id, ICON_WARNING + tr(user_locale, _(" Faqat docx formatli fayl qabul qilinadi")))
            return
        try:
            questions = read_question_docx(question_file)
            right_answers = read_answers(answer_file)
        except Exception as er:
            print(er)
        if len(questions) != len(right_answers):
            send_message(chat_id, tr(user_locale, _("Testlar soni bilan javoblar soni bir hil emas, iltimoz to'g'irlab qayta jo'nating")))
            context.user_data['doc'] = DOC_INIT
        else:
            test_group_id = context.user_data['test_group_id']
            if db.save_tests(test_group_id, questions, right_answers):
                send_message(chat_id, ICON_SUCCESS + tr(user_locale, _(" Testlar muvaffaqiyatli kiritildi")))
                if os.path.exists(question_file):
                    os.remove(question_file)
                if os.path.exists(answer_file):
                    os.remove(answer_file)
            else:
                send_message(chat_id, tr(user_locale, _("Testlarni saqlashda xatolik yuz berdi")))


def monitor_start(update: Updater, context, exam_id):
    monitor_stop(context)
    try:
        if not exam_id:
            return
        # send_exam_result_info(update, context, exam_id)
        context.bot_data['exam_id'] = exam_id
        context.bot_data['chat_id'] = update.effective_chat.id
        context.bot_data['message_id'] = update.effective_message.message_id

        #  delete 'current_time' to start from begin
        if 'current_time' in context.bot_data:
            del context.bot_data['current_time']
        new_job = context.job_queue.run_repeating(send_monitor_info, interval=5, first=0, context=context)
        context.chat_data['job'] = new_job
    except Exception as ex:
        logger.error(ex)


def monitor_stop(context):
    try:
        if 'job' in context.chat_data:
            job = context.chat_data['job']
            job.schedule_removal()
            del context.chat_data['job']
    except Exception as ex:
        logger.error(ex)


def send_exam_result_info(update, context, exam_id):
    """
    :param update:
    :param exam_id:
    :param context:
    :return: Jadvalga izoh
    """
    global db, bot
    try:
        chat_id = update.effective_chat.id
        user_locale = get_user_locale(chat_id, context)
        exam = db.get_exam_time(exam_id)
        exam_time = exam['time'] if exam['time'] else tr(user_locale, _("Belgilanmagan"))
        duration = exam['duration'] if exam['duration'] else tr(user_locale, _("Belgilanmagan"))
        bot.send_message(chat_id=update.effective_chat.id,
                         text=tr(user_locale, _("\nImtihon vaqti: {}. \nDavomiyligi (min): {}\n "
                                                "{} - Yechilgan test soni, \
                                                n{} - To'g'ri yechilgan test soni, \n{} - Natija foizda"))
                         .format(exam_time, duration, ICON_WRITING, ICON_CHECK, ICON_WIN)
                         )
    except Exception as ex:
        logger.error(ex)


def send_monitor_info(context):
    """Send the alarm message."""
    try:
        exam_id = context.bot_data['exam_id']
        chat_id = context.bot_data['chat_id']
        message_id = context.bot_data['message_id']
        if 'current_time' not in context.bot_data:
            context.bot_data['current_time'] = datetime.datetime.now()
        else:
            cur_time = context.bot_data['current_time']
            context.bot_data['current_time'] = datetime.datetime.now()
            if not db.has_exam_changes(cur_time, exam_id):
                # print('no changes, class-id: ', class_id)
                return

        result = get_class_marks(chat_id, context, exam_id)

        user_locale = get_user_locale(chat_id, context)
        button_list = [[InlineKeyboardButton(tr(user_locale, _("Tahlili")), callback_data=json.dumps(
                           {"key": KEY_EXAM_MONITOR, "action": ACTION_EXAM_ANALYZE, "exam_id": exam_id}))]]
        reply_markup = InlineKeyboardMarkup(button_list)
        # !@#
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text="{}\n{}".format(result, time.strftime("%I:%M:%S", time.localtime())),
                              parse_mode=ParseMode.HTML, reply_markup=reply_markup)
    except Exception as ex:
        logger.error(ex)


def get_class_marks(chat_id, context, exam_id):
    rows = db.get_marks(exam_id)

    user_locale = get_user_locale(chat_id, context)
    if not rows:
        return tr(user_locale, _("Hozircha hech kim yo'q"))

    tests_num = db.get_test_number(exam_id)
    fio = None
    table = "\n\n<pre>|{0:^13}|{1:^3}|{2:^2}|{3:^2}|</pre>".format(
        tr(user_locale, _("Ism sharifi"))[0:12], ICON_WRITING, ICON_CHECK, ICON_WIN
    )
    try:
        for row in rows:
            fio = '{} {}.'.format(row['last_name'], row['first_name'].strip()[0:1].upper())
            if row['right']:
                mark = int(row['right'] * 100 / tests_num)
            else:
                mark = "-"
            right_answers = row['right'] if row['right'] else "-"
            table += "\n<pre>|{0:<13}|{1:^3}|{2:^3}|{3:^3}|</pre>". \
                format(fio[0:12], row['answers'], right_answers, mark)
        return table
    except Exception as ex:
        logger.error(ex)


def button(user_locale, button_constant):
    if button_constant == BUTTON_REGISTRATION:
        value = " " + tr(user_locale, _("Ro'yxatdan o'tish"))
        return ICON_WRITING + value
    elif button_constant == BUTTON_EXAM:
        value = " " + tr(user_locale, _("Imtihon"))
        return ICON_EXAM + value
    elif button_constant == BUTTON_TEST:
        value = " " + tr(user_locale, _("Test"))
        return ICON_SETTING + value
    elif button_constant == BUTTON_TEST_INSERT:
        value = " " + tr(user_locale, _("Yozib kiritish"))
        return ICON_ADD + value
    elif button_constant == BUTTON_TEST_VIEW:
        value = " " + tr(user_locale, _("Ko'rish"))
        return ICON_VIEW + value
    elif button_constant == BUTTON_TEST_DELETE:
        value = " " + tr(user_locale, _("O'chirish"))
        return ICON_DELETE + value
    elif button_constant == BUTTON_TEST_INSERT_FILE:
        value = " " + tr(user_locale, _("Fayl yordamida kiritish"))
        return ICON_FILE + value
    elif button_constant == BUTTON_BACK:
        value = " " + tr(user_locale, _("Orqaga"))
        return ICON_LEFT + value
    elif button_constant == BUTTON_MONITOR_BACK:
        value = " " + tr(user_locale, _("Orqaga"))
        return ICON_LEFT + value
    elif button_constant == BUTTON_MONITOR_PLAY:
        value = " " + tr(user_locale, _("Boshlash"))
        return ICON_PLAY + value
    elif button_constant == BUTTON_MONITOR_STOP:
        value = " " + tr(user_locale, _("Tugatish"))
        return ICON_STOP + value


def get_user_locale(chat_id, context):
    global db

    if not isinstance(context.user_data, dict):
        context.bot_data['locale'] = db.get_user_locale(chat_id)
        return context.bot_data['locale']

    if 'locale' not in context.user_data.keys():
        context.user_data['locale'] = db.get_user_locale(chat_id)
    return context.user_data['locale']


def tr(user_locale, text):
    global _ru, _uz, _us

    if user_locale == 'uz':
        return _uz(text)
    elif user_locale == "ru":
        return _ru(text)
    else:
        return _us(text)


def _(value):
    """
    Tarjima qilinishi uchun
    :param value - string
    """
    return value


def error(update, context):
    # add all the dev user_ids in this list. You can also add ids of channels or groups.
    devs = [57018741]
    # we want to notify the user of this problem. This will always work, but not notify users if the update is an
    # callback or inline query, or a poll update. In case you want this, keep in mind that sending the message
    # could fail
    if update and update.effective_message:
        text = "Hey. I'm sorry to inform you that an error happened while I tried to handle your update. " \
               "My developer(s) will be notified."
        update.effective_message.reply_text(text)
    # This traceback is created with accessing the traceback object from the sys.exc_info, which is returned as the
    # third value of the returned tuple. Then we use the traceback.format_tb to get the traceback as a string, which
    # for a weird reason separates the line breaks in a list, but keeps the linebreaks itself. So just joining an
    # empty string works fine.
    trace = "".join(traceback.format_tb(sys.exc_info()[2]))
    # lets try to get as much information from the telegram update as possible
    payload = ""
    # normally, we always have an user. If not, its either a channel or a poll update.
    if update and update.effective_user:
        payload += f' with the user {mention_html(update.effective_user.id, update.effective_user.first_name)}'
    # there are more situations when you don't get a chat
    if update.effective_chat:
        payload += f' within the chat <i>{update.effective_chat.title}</i>'
        if update.effective_chat.username:
            payload += f' (@{update.effective_chat.username})'
    # but only one where you have an empty payload by now: A poll (buuuh)
    if update.poll:
        payload += f' with the poll id {update.poll.id}.'
    # lets put this in a "well" formatted text
    text = f"Hey.\n The error <code>{context.error}</code> happened{payload}. The full traceback:\n\n<code>{trace}" \
           f"</code>"
    # and send it to the dev(s)
    for dev_id in devs:
        context.bot.send_message(dev_id, text, parse_mode=ParseMode.HTML)
    # we raise the error again, so the logger module catches it. If you don't use the logger module, use it.
    raise


def main():
    global token, bot, _ru, _uz, _us
    _ru, _uz, _us = init_locales("teacher")

    logger.info("starting the bot")
    updater = Updater(token=token, use_context=True)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CallbackQueryHandler(handle_inline_keyboard))
    updater.dispatcher.add_handler(MessageHandler(Filters.text, handle_text))
    updater.dispatcher.add_handler(MessageHandler(Filters.document, handle_document))
    # updater.dispatcher.add_handler(MessageHandler(Filters.poll, handle_poll))
    # updater.dispatcher.add_handler(CommandHandler('help', help))
    updater.dispatcher.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    bot = Bot(token=token)
    bot.send_message(chat_id=57018741, text='Bot ishga tushirildi')
    # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT
    updater.idle()


if __name__ == '__main__':
    logger = set_logger(logging, 'Teacher')

    with open('config.json', 'r') as file:
        config = json.load(file)

    logger.info("reading database")
    token = config['teacher_token']
    db = DBManager(config['db'])
    main()
