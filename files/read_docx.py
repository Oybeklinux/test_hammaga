# import docx
import re
from docx2python import docx2python
import logging
from general_functions import set_logger
logger = set_logger(logging, 'DocxReader')


def get_position(letter):
    pos = 1
    for bullet in ['aAаА','bBбБ', 'вВvVsSсСcC', 'гГgGdD']:
        if letter in bullet:
            return pos
        pos += 1


def split_test(text):
    """
    savolni alohida javoblarni alohida qilib qaytaradi
    :param test_text - obyekt is_list va answer hususiyatiga ega. Agar javoblarda a,b,s va hak. variant bo'lmasa,
        u holda is_list husisiyatidan foydalanib javoblar ajratiladi
    :return savol javoblarni strukturlab qaytaradi
    """
    test = {'question': '', 'answer': []}
    pattern = '\d+(\.|\))\s*(?P<question>.*?)(?=a(\.|\)))(?P<answers>.*)'
    pattern_answer = '(?:[absdeабвгдvgdc])(?:\.|\))(.*?)(?=[absdeабвгдvgdc](?:\.|\))|$)'
    result = re.search(pattern, text, re.U | re.I | re.S)
    if result:
        question = result.group('question').strip()
        answer = result.group('answers').strip()
        test['question'] = question
        for ans in re.findall(pattern_answer, answer, re.U | re.I | re.S):
            test['answer'].append(ans.strip())
        return test


def read_question_docx(file):
    """
    docx fayldan har bitta test savoli va unga tegishli javoblarni alohida list qilib oladi
    har bitta natijani split_test funksiyaga beradi
    :param file - savollardan iborat fayl
    :return savol javoblarni strukturlab qaytaradi
    """
    try:
        html = docx2python(file)
        question_list = html.body[0][0][0]
    except Exception as error:
        logger.error(error)
        return None
    tests = []
    text = ''
    pattern = '(?P<test>\d+(\.|\)).*?)(?=\d+(\.|\)))'
    for question in question_list:
        text += '\n{}'.format(question)
        result = re.search(pattern, text, re.U | re.I | re.S)
        if result:
            test = split_test(result.group('test'))
            if test:
                tests.append(test)
            text = text[result.end():]
    if text.strip():
        pattern = '(?P<test>\d+(\.|\)).*?)(?=\d+(\.|\))|$)'
        result = re.search(pattern, text, re.U | re.I | re.S)
        if result:
            test = split_test(result.group('test'))
            if test:
                tests.append(test)
    return tests





def read_answers(file):
    """
    Worddan har bitta testning to'g'ri javobini list qilib oladi
    Har bitta 'List Paragraph' bo'lgan variant oladi, aks holda reg exp bilan oladi
    :param file - savollardan iborat fayl
    :return test javoblarni list shaklida qaytaradi
    """
    try:
        html = docx2python(file)
        answer_list = html.body[0][0][0]
    except Exception as error:
        logger.error(error)
        return None
    keys = []
    for answer in answer_list:
        pattern_variant = '\d+(?:\.|\))\s*(\w)'
        for variant in re.findall(pattern_variant, answer, re.I | re.U | re.S):
            keys.append(get_position(variant))
    return keys

# dir = "/home/myprojects/python/projects/oquvchi_test/files/"
# dir = "/home/user/Desktop/temp/"
# file = "57018741"
# file = "14"
# i = 1
# for test in read_question_docx(dir + file + "_question.docx"):
#     print(i, test)
#     i += 1
# i = 1
# for answer in read_answers(dir + file + "_answer.docx"):
#     print(i, answer)
#     i += 1
