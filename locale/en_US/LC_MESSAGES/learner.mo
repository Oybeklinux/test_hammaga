��    %      D  5   l      @  8   A  Z   z  �   �  �   V            �   -     �     �     �     �  2     '   :  ;   b     �     �     �     �     �     �  E     -   H     v      �  "   �     �  
   �     �     �     
  Q        i      y     �  (   �  `   �  �   A	  ;   <
  l   x
  �   �
  �   w          (  g   B     �     �     �     �  <   �  "   9  .   \     �     �     �     �     �     �  <   �  '   '     O      W      x     �     �     �     �     �  7   �          #     @  1   ^  V   �                                  "                                                                         
                                #   $      !         %            	          

*Savol {}*                      {} {}
{}
   {} {}{} {} 

*Savollar soni:* {}
*To'g'ri javoblar soni:* {}
*Natija:* {} %
*Umumiy ketgan vaqt:* {}  

Imtihon sanasi: *{}*
Imtihon nomi: *{}* 
Savollar soni: {}
To'g'ri javoblar soni: *{}*
Testga ketgan vaqt: *{}*
Natija: *{} %* *Asosiy bo'lim*:

- Umumiy test ballarni ko'rmoqchi bo'lsangiz *{}* tugmasini bosing. 
   - Agar test imtihonidagi xatolaringiz ustida ishlamoqchi bo'lsangiz *{}* tugmasini bosing.
 *Hammasi to'g'ri* *Hammasi xato* *Test bo'limi*:

️   - Test topshirish uchun *{}* tugmasini bosing. 
️   - Test natijalarini ko'rish uchun *{}* tugmasini bosing.
 Asosiy menyuga Familiyangizni kiriting Familiyani o'zgartirish Imtihon boshlanmadi Imtihon boshlanmadi. Boshlansa, sizga habar keladi Imtihon ro'yxatidan birini tanlang 👇 Imtihon topshirish uchun muallim sizni taklif qilishi kerak Imtihon tugadi Imtihon tugadi.  Ismingizni kiriting Ismni o'zgartirish Kontaktni jo'natish Ma'lumot O'qituvchi: *{} {}*
Imtihon nomi: *{}*
Imtihon boshlanish vaqti: *{}* O'qituvchi: {} {}
Imtihon nomi: {}
Holati: {} Ro'yxatdan o'tish Siz hali imtihon topshirmagansiz Siz imtihonni topshirib bo'lgansiz Sizning natijangiz
 Sozlamalar Test ballarim Test natijalarim Test tahlili Test topshirish uchun ro'yxatdan o'ting. 
Buning uchun *{}* tugmasini bosing 👇 Testni boshlash Topshirilgan imtihon mavjud emas Use /start to test this bot. {} savoldan *{} to'g'ri javob* berdingiz {}Siz ro'yxatdan muvaffaqiyalti o'tdingiz.{}
 Imtihonga tayyor bo'lsangiz, *{}* tugmasini bosing Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-11 19:44+0500
Last-Translator: 
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
 

*Question {}*                      {} {}
{}
   {} {}{} {} 

* Number of questions: * {}
* Number of correct answers: * {}
*The result:* {} %
* Total time spent: * {}  

Exam Date: * {} *
Exam Name: * {} *
Number of questions: {}
Number of correct answers: * {} *
Time spent on the test: * {} *
The result: *{} %* * Main section *:

- Press * {} * if you want to see the total test scores.
   - If you want to work on your mistakes in the test exam, press * {} *.
 * All answers are right * * All answers are wrong * * Test department *:

️ - Press * {} * to take the test.
️ - Press * {} * to see the test results.
 To the main menu Enter your last name Change the last name The exam did not start The exam did not start. When it starts, you will be notified Select one from the exam list 👇 The teacher should invite you to take the exam The exam is over The exam is over.  Enter your name Change the name Send a contact Information Teacher: * {} {} *
Exam Name: * {} *
Exam start time: * {} * Teacher: {} {}
Exam Name: {}
Status: {} Sign up You have not passed the exam yet You have already passed the exam Your result
 Settings My test scores My test results Test analysis Sign up to take the test.
To do this, press * {} * 👇 Start the test Passed exam is not available Use /start to start this bot. You answered * {} out of {} questions correctly * {}You have successfully registered. {}
  When you are ready for the exam, press * {} * 