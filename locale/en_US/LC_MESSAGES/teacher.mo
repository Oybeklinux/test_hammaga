��    =        S   �      8  �   9  (   �  (         8  !   Y     {  N   �  #   �  /        1  G   L     �     �  2  �      �      �  H    	  z   i	     �	     �	     
     +
     3
  q   E
     �
     �
  /   �
       H   !  /   j     �     �  	   �     �  (   �  	   �  *   �     *  :   1  1   l     �     �  H   �  u   �  3   u  S   �  V   �     T     Y  U   y  �   �  Q   �  �     %   �  !   �  9   �          !     3     @  �   O  �   J  $   �  !   �          5     T  G   g  )   �  8   �       A   -     o     }    �  (   �     �  4   �  �         �     �     �     �     �  V        ]     n     �     �  G   �     �  	        "     2     B     G     e     l     �  /   �  -   �     �     �  4      q   5  (   �  ;   �  N        [  "   `  E   �  �   �  Q   |  �   �  (   e  $   �  1   �     �     �     �                               &      3   2   $          0           '       ,   9             :       "   <   =            /       7       1      +         8         #      6   %      -             )                (      
      4       	       *              ;                         5   .   !               
Imtihon vaqti: {}. 
Davomiyligi (min): {}
 {} - Yechilgan test soni,                                                 n{} - To'g'ri yechilgan test soni, 
{} - Natija foizda  Faqat docx formatli fayl qabul qilinadi  Siz ro'yxatdan muvaffaqiyatli o'tdingiz  Sizda test to'plami mavjud emas  Testlar muvaffaqiyatli kiritildi  Xatolik yuz berdi *{}* test to'plamiga tegishli qaysi testni o'chirmoqchisiz? Bittasini tanlang  1. Testlar yozilgan faylni yuboring 2. Endi test javoblari yozilgan faylni yuboring Amallardan birini tanlang  Avval test to'plamini tuzib oling. Buni *{}* bo'limida amalga oshirasiz Belgilanmagan Boshlash Bu dastur turli sohadagi ta'lim beruvchilar uchun tuzilgan bo'lib, unda *ta'lim oluvchining bilimini tekshirish jarayoni avtomatlashtirilgan*.  Hozircha bilimni tekshirish test asosida olib boriladi. Keyinchalik esa  ta'lim beruvchilar talabiga binoan o'zgartirishlar, qo'shimcha imkoniyatlar  kiritiladi.
 Bu funktsiya xali ishga tushmadi Bu imtihonda testlar mavjud emas Dasturdan foydalanish uchun *{button}* tugmasini bosib ro'yxatdan o'ting Endi imtihon nomini kiriting. Nom ma'noli va hamma imtihondan farqli bo'lsa, ularni bir-biridan ajratishingiz oson bo'ladi Familiyangizni kiriting Fayl yordamida kiritish Hozircha hech kim yo'q Imtihon Imtihon boshlandi Imtihon jarayonini ko'rmoqchi bo'lsangiz *{}* tugmasini bosing.
Testlar bilan ishlash uchun *{}* tugmasini bosing Imtihon tugadi Imtihonga taklif qilish Imtihonni boshlash uchun *{}* tugmasini bosing. Imtihonni kuzatish Imtihonni qaysi test to'plami asosida o'tkazmoqchisiz? Bittasini tanlang Imtihonni tugatish uchun *{}* tugmasini bosing. Ism sharifi Ismingizni kiriting Kasbingiz Ko'rish Ko'rish uchun testlar to'plamini tanlang O'chirish O'chirish uchun testlar to'plamini tanlang Orqaga Qaysi imtihon havolasini yubormoqchisiz? Bittasini tanlang Qaysi imtihonni kuzatmoqchisiz? Bittasini tanlang Ro'yxatdan o'tish Savol Sizda imtihonlar mavjud emas. Imtihon tuzish uchun *{}* tugmasini bosing Sizda imtihonlar mavjud emas. Imtihon tuzish uchun quyidagi tugmalarni ketma-ketlikda bosing:
1. *{}*
2. *{}*
3. *{}* Sizda test to'plami mavjud emas. Avval uni kiriting Sizda test to'plami mavjud emas. Test to'plamini tuzish uchun *{}* tugmasini bosing Sizni {} "{}" nomli imtihonga taklif qilmoqda
t.me/BilimingniTekshirTalabaBot?start={} Test Test muvaffaqiyatli o'chirildi. Test o'quvchilar tarafidan imtihonda ishlatilganligi uchun uni o'chirish mumkin emas. Test sahifasiga hush kelibsiz.
Testlarni bittalab kiritish uchun *{}* tugmasini bosing
Testlaringiz docx formatli faylda bo'lsa, *{}* tugmasini bosib, avval testlar yozilgan faylni, keyin alohida javoblar yozilgan faylni yuboring. Testlar soni bilan javoblar soni bir hil emas, iltimoz to'g'irlab qayta jo'nating Testlarni fayl ko'rinishda yuborish ikki qadamdan iborat:
  1. Testlar yozilgan faylni yuborish
  2. Test javoblari yozilgan faylni yuborish. Testlarni saqlashda xatolik yuz berdi Testni o'chirishda xatolik bo'ldi Testni qaysi test to'plamiga qo'shasiz? Bittasini tanlang Tugatish Xatolik yuz berdi Yangi tuzish Yozib kiritish Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-13 00:06+0500
Last-Translator: 
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
 
Exam time: {}.
Duration (min): {}
{} - Number of tests solved,
{} - Number of correctly solved tests,
{} - The result  (in percent)  Only docx format files are accepted  You have successfully registered  The test set is not available  Tests were successfully saved  An error occurred *{}* Which test do you want to delete that belongs to *{}*? Choose one  1. Send a file in which tests are written 2. And now send a file in which test answers are written Choose one of the actions  Create a test set first. You can do this in the section of * {} * Not specified Get started This program is designed for educators in various fields, in which * the process of testing the knowledge of the learner is automated *. For now, knowledge testing is done on a test basis. Later, at the request of educators, changes and additional opportunities will be introduced.
 This function has not yet been activated The test set is not available To use the program, register by pressing *{button} * Now enter the name of the exam. If the name is meaningful and different from all the exams, it will be easier to distinguish them from each other Enter your last name Enter using a file There is no one yet Exam The exam has begun If you want to observe the exam process, press *{}*.
Press *{}* to work with the tests The exam is over Invitation to the exam Press *{}* to start the exam. Exam observation On the basis of which test set do you want to take the exam? Choose one Press *{}* to end the exam. Last name Enter your name Your occupation View Select a set of tests to view Delete Select a set of tests to delete Back Which exam link do you want to send? Choose one Which exam do you want to observe? Choose one Sign up Question You don’t have exams. Press *{}* to create an exam You don’t have exams. To create an exam, press the following buttons in sequence:
1. * {} *
2. * {} *
3. * {} * You do not have a test set. Add it first You do not have a test set. Press *{}* to create a test set {} Invites you to an exam named "{}"
t.me/BilimingniTekshirTalabaBot?start= {} Test The test was successfully deleted. Since the test is used by students in the exam, it cannot be deleted. Welcome to the test page.
Press *{}* to enter the tests one by one
If your tests are in a docx file, then press *{}* to send the test file, then send the answers file seperately. The number of tests differs from the number of answers, please correct and resend Sending the tests by file consists of two steps:
  1. Sending a file in which tests are written
  2. Sending a file in which test answers are written. An error occurred while saving the tests There was an error deleting the test Which test set do you add the test to? Choose one Stop An error occurred Create a new one Add by typing 