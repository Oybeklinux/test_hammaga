��    =        S   �      8  �   9  (   �  (         8  !   Y     {  N   �  #   �  /        1  G   L     �     �  2  �      �      �  H    	  z   i	     �	     �	     
     +
     3
  q   E
     �
     �
  /   �
       H   !  /   j     �     �  	   �     �  (   �  	   �  *   �     *  :   1  1   l     �     �  H   �  u   �  3   u  S   �  V   �     T     Y  U   y  �   �  Q   �  �     %   �  !   �  9   �          !     3     @  �   O    J  5   S  9   �  1   �  -   �      #  p   D  0   �  E   �  0   ,  {   ]     �     �  �  �  :   �  0   +  o   \  �   �  &   �  +   �     �          *  �   H     �  *     >   1  ,   p  �   �  D   #     h     �     �     �  B   �       V     
   t  g     S   �     ;     R  b   _  �   �  V   ~  ~   �  `   T      �   %   �   �   �   }  j!  �   �"  �   �#  H   ^$  B   �$  _   �$     J%     a%     �%     �%                          &      3   2   $          0           '       ,   9             :       "   <   =            /       7       1      +         8         #      6   %      -             )                (      
      4       	       *              ;                         5   .   !               
Imtihon vaqti: {}. 
Davomiyligi (min): {}
 {} - Yechilgan test soni,                                                 n{} - To'g'ri yechilgan test soni, 
{} - Natija foizda  Faqat docx formatli fayl qabul qilinadi  Siz ro'yxatdan muvaffaqiyatli o'tdingiz  Sizda test to'plami mavjud emas  Testlar muvaffaqiyatli kiritildi  Xatolik yuz berdi *{}* test to'plamiga tegishli qaysi testni o'chirmoqchisiz? Bittasini tanlang  1. Testlar yozilgan faylni yuboring 2. Endi test javoblari yozilgan faylni yuboring Amallardan birini tanlang  Avval test to'plamini tuzib oling. Buni *{}* bo'limida amalga oshirasiz Belgilanmagan Boshlash Bu dastur turli sohadagi ta'lim beruvchilar uchun tuzilgan bo'lib, unda *ta'lim oluvchining bilimini tekshirish jarayoni avtomatlashtirilgan*.  Hozircha bilimni tekshirish test asosida olib boriladi. Keyinchalik esa  ta'lim beruvchilar talabiga binoan o'zgartirishlar, qo'shimcha imkoniyatlar  kiritiladi.
 Bu funktsiya xali ishga tushmadi Bu imtihonda testlar mavjud emas Dasturdan foydalanish uchun *{button}* tugmasini bosib ro'yxatdan o'ting Endi imtihon nomini kiriting. Nom ma'noli va hamma imtihondan farqli bo'lsa, ularni bir-biridan ajratishingiz oson bo'ladi Familiyangizni kiriting Fayl yordamida kiritish Hozircha hech kim yo'q Imtihon Imtihon boshlandi Imtihon jarayonini ko'rmoqchi bo'lsangiz *{}* tugmasini bosing.
Testlar bilan ishlash uchun *{}* tugmasini bosing Imtihon tugadi Imtihonga taklif qilish Imtihonni boshlash uchun *{}* tugmasini bosing. Imtihonni kuzatish Imtihonni qaysi test to'plami asosida o'tkazmoqchisiz? Bittasini tanlang Imtihonni tugatish uchun *{}* tugmasini bosing. Ism sharifi Ismingizni kiriting Kasbingiz Ko'rish Ko'rish uchun testlar to'plamini tanlang O'chirish O'chirish uchun testlar to'plamini tanlang Orqaga Qaysi imtihon havolasini yubormoqchisiz? Bittasini tanlang Qaysi imtihonni kuzatmoqchisiz? Bittasini tanlang Ro'yxatdan o'tish Savol Sizda imtihonlar mavjud emas. Imtihon tuzish uchun *{}* tugmasini bosing Sizda imtihonlar mavjud emas. Imtihon tuzish uchun quyidagi tugmalarni ketma-ketlikda bosing:
1. *{}*
2. *{}*
3. *{}* Sizda test to'plami mavjud emas. Avval uni kiriting Sizda test to'plami mavjud emas. Test to'plamini tuzish uchun *{}* tugmasini bosing Sizni {} "{}" nomli imtihonga taklif qilmoqda
t.me/BilimingniTekshirTalabaBot?start={} Test Test muvaffaqiyatli o'chirildi. Test o'quvchilar tarafidan imtihonda ishlatilganligi uchun uni o'chirish mumkin emas. Test sahifasiga hush kelibsiz.
Testlarni bittalab kiritish uchun *{}* tugmasini bosing
Testlaringiz docx formatli faylda bo'lsa, *{}* tugmasini bosib, avval testlar yozilgan faylni, keyin alohida javoblar yozilgan faylni yuboring. Testlar soni bilan javoblar soni bir hil emas, iltimoz to'g'irlab qayta jo'nating Testlarni fayl ko'rinishda yuborish ikki qadamdan iborat:
  1. Testlar yozilgan faylni yuborish
  2. Test javoblari yozilgan faylni yuborish. Testlarni saqlashda xatolik yuz berdi Testni o'chirishda xatolik bo'ldi Testni qaysi test to'plamiga qo'shasiz? Bittasini tanlang Tugatish Xatolik yuz berdi Yangi tuzish Yozib kiritish Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-13 00:05+0500
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
 
Время экзамена: {}. 
Продолжительность (мин): {} 
{} - Количество решенных тестов, 
{} - Количество правильно решенных тестов, 
{} - Результат в процентах Принимаются только docx формат  Вы успешно зарегистрировались  У вас нет тестового набора  Тесты успешно сохранены  Произошла ошибка Какой тест из набора тестов *{}* вы хотите удалить? Выбери один  1. Отправьте файл с тестами 2. Отправьте файл с тестовыми ответами Выберите одно из действий  Сначала создайте тестовый набор. Вы можете сделать это в разделе *{}* Не указан Начать Эта программа предназначена для преподавателей в различных областях.  * автоматизирован процесс проверки знаний обучаемого *.  Пока проверка знаний проводится на тестовой основе. А потом  По желанию воспитателей будут внесены изменения, введены дополнительные возможности.
 Эта функция еще не активирована У вас нет тестового набора Чтобы использовать программу зарегистрируйтесь нажав *{button}* Теперь введите название экзамена. Если имя значимое и отличается от всех экзаменов, их будет легче отличить друг от друга Введите свою фамилию Введите с помощью файла Пока нет никого Экзамен Экзамен начался Если вы хотите увидеть процесс экзамена, нажмите * {} *. 
 Для работы с тестами нажмите * {} * Экзамен окончен Приглашение на экзамен Нажмите * {} *, чтобы начать экзамен. Наблюдение за экзаменом На основании какого набора тестов вы хотите сдавать экзамен? Выбери один Нажмите * {} *, чтобы завершить экзамен. Имя и фамилия Введите ваше имя Ваша профессия Увидеть Выберите набор тестов для просмотра Удалить Выберите набор тестов, которые нужно отключить Назад Какую ссылку на экзамен вы хотите отправить? Выбери один Какой экзамен вы хотите смотреть? Выбери один Регистрация Вопрос У вас нет экзаменов. Нажмите *{}*, чтобы создать экзамен У вас нет экзаменов. Чтобы создать экзамен, последовательно нажмите следующие кнопки:
1. * {} *
2. * {} *
3. * {} * У вас нет тестового набора. Сначала введите его У вас нет тестового набора. Нажмите * {} *, чтобы создать тестовый набор Приглашает вас на экзамен {} "{}"
t.me/BilimingniTekshirTalabaBot?start={} Тест Тест успешно удален. Поскольку тест используется студентами на экзамене, его нельзя  удалить. Добро пожаловать на страницу ввода теста. 
  Нажмите * {} *, чтобы ввести тесты один за другим 
  Если ваши тесты находятся в файле docx, сначала нажмите * {} *  отправьте тестовый файл, затем файл с отдельными ответами. Количество тестов и количество ответов не совпадают, исправьте и отправьте повторно Отправка тестов в виде файла состоит из двух шагов:
  1. Отправка файла с тестами
  2. Отправка файла с тестовыми ответами. Произошла ошибка при сохранении тестов При удалении теста произошла ошибка В какой набор тестов вы добавляете тест? Выбери один Прекращение Произошла ошибка Создать новый Запись ввода 