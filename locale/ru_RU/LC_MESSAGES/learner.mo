��    %      D  5   l      @  8   A  Z   z  �   �  �   V            �   -     �     �     �     �  2     '   :  ;   b     �     �     �     �     �     �  E     -   H     v      �  "   �     �  
   �     �     �     
  Q        i      y     �  (   �  `   �  �   A	  ?   <
  �   |
    >  %  K     q  !   �  �   �     i  &   �     �  "   �  r   �  1   _  W   �     �  %   	     /     N  !   i     �  t   �  L        b  *   y  %   �     �     �     �       #   (  r   L     �  #   �  Q   �  M   K  �   �                                  "                                                                         
                                #   $      !         %            	          

*Savol {}*                      {} {}
{}
   {} {}{} {} 

*Savollar soni:* {}
*To'g'ri javoblar soni:* {}
*Natija:* {} %
*Umumiy ketgan vaqt:* {}  

Imtihon sanasi: *{}*
Imtihon nomi: *{}* 
Savollar soni: {}
To'g'ri javoblar soni: *{}*
Testga ketgan vaqt: *{}*
Natija: *{} %* *Asosiy bo'lim*:

- Umumiy test ballarni ko'rmoqchi bo'lsangiz *{}* tugmasini bosing. 
   - Agar test imtihonidagi xatolaringiz ustida ishlamoqchi bo'lsangiz *{}* tugmasini bosing.
 *Hammasi to'g'ri* *Hammasi xato* *Test bo'limi*:

️   - Test topshirish uchun *{}* tugmasini bosing. 
️   - Test natijalarini ko'rish uchun *{}* tugmasini bosing.
 Asosiy menyuga Familiyangizni kiriting Familiyani o'zgartirish Imtihon boshlanmadi Imtihon boshlanmadi. Boshlansa, sizga habar keladi Imtihon ro'yxatidan birini tanlang 👇 Imtihon topshirish uchun muallim sizni taklif qilishi kerak Imtihon tugadi Imtihon tugadi.  Ismingizni kiriting Ismni o'zgartirish Kontaktni jo'natish Ma'lumot O'qituvchi: *{} {}*
Imtihon nomi: *{}*
Imtihon boshlanish vaqti: *{}* O'qituvchi: {} {}
Imtihon nomi: {}
Holati: {} Ro'yxatdan o'tish Siz hali imtihon topshirmagansiz Siz imtihonni topshirib bo'lgansiz Sizning natijangiz
 Sozlamalar Test ballarim Test natijalarim Test tahlili Test topshirish uchun ro'yxatdan o'ting. 
Buning uchun *{}* tugmasini bosing 👇 Testni boshlash Topshirilgan imtihon mavjud emas Use /start to test this bot. {} savoldan *{} to'g'ri javob* berdingiz {}Siz ro'yxatdan muvaffaqiyalti o'tdingiz.{}
 Imtihonga tayyor bo'lsangiz, *{}* tugmasini bosing Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-11 20:12+0500
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
 

*Вопрос {}*                      {} {}
{}
   {} {}{} {} 

* Количество вопросов: * {}
* Количество правильных ответов: * {}
*Результат:* {} %
* Общее затраченное время: * {}  

Дата экзамена: * {} *
Название экзамена: * {} *
Количество вопросов: {}
Количество правильных ответов: * {} *
Время, затраченное на тест: * {} *
Результат: *{} %* *Основной раздел*:

- Нажмите * {} *, если хотите увидеть общие результаты теста.
   - Если вы хотите поработать над своими ошибками на тестовом экзамене, нажмите * {} *.
 * Все правильно * * Все неправильно * * Раздел тест *:

️   - Нажмите * {} *, чтобы пройти тест.
️   - Нажмите * {} *, чтобы увидеть результаты теста.
 В главное меню Введите свою фамилию Сменить фамилию Экзамен не начался Экзамен не начался. Когда он начнется, вы получите уведомление Выберите один из экзаменов Преподаватель должен пригласить вас на экзамен Экзамен закончен Экзамен закончился.  Введите ваше имя Переименовать Отправить контакт Информация Учитель: * {} {} *
Название экзамена: * {} *
Время начала экзамена: * {} * Учитель: {} {}
Название экзамена: {}
Статус: {} Регистрация Вы еще не сдали экзамен Вы уже сдали экзамен Ваш результат
 Настройки Мои оценки Мои результаты Анализ результатов Зарегистрируйтесь, чтобы пройти тест.
Для этого нажмите * {} * 👇 Начать тест Вы не сдали экзамен Используйте /start, чтобы запустить этого бота. Вы правильно ответили * на {} вопросов из {} * {} Вы успешно зарегистрировались. {}
  Когда будете готовы к экзамену, нажмите * {} * 