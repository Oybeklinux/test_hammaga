from telegram.ext import Updater
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from constants import *
import datetime
import json
import gettext


def send_languages(chat_id, bot):
    languages = [{'name': "O'zbek", 'lang': 'uz', 'flag': ICON_UZ},
                 {'name': "Русский", 'lang': 'ru', 'flag': ICON_RU},
                 {'name': "English", 'lang': 'us', 'flag': ICON_US}]
    button_list = []
    for language in languages:
        callback_data = json.dumps({"key": KEY_LANGUAGE, "locale": language['lang']})
        button_list.append([InlineKeyboardButton(
            '{} {}'.format(language['flag'], language['name']),
            # callback_data='{}:{}'.format("language", language['lang']))])
            callback_data=callback_data)])
    reply_markup = InlineKeyboardMarkup(button_list)

    bot.send_message(chat_id=chat_id,
                     text="Tilni tanlang\n\nВыберите язык",
                     reply_markup=reply_markup)


def init_locales(name):
    gettext.bindtextdomain(name, "/locale")
    gettext.textdomain(name)
    language_ru = gettext.translation(name, localedir="locale", languages=[LOCALES['ru']])
    language_ru.install()
    _ru = language_ru.gettext
    language_uz = gettext.translation(name, localedir="locale", languages=[LOCALES['uz']])
    language_uz.install()
    _uz = language_uz.gettext
    language_us = gettext.translation(name, localedir="locale", languages=[LOCALES['en']])
    language_us.install()
    _us = language_us.gettext
    return _ru, _uz, _us


def send_exam_list(update: Updater, db, bot, chat_id=None):
    rows = db.get_exam_list(chat_id=chat_id)
    lst = []
    if not rows:
        bot.send_message(chat_id=update.effective_chat.id,
                         text="Topshirilgan imtihon mavjud emas")
        return
    for row in rows:
        print(row['name'])
        lst.append(InlineKeyboardButton(text='{} {}'.format(row['name'], row['date']),
                                        callback_data="theme:{}".format(row['id'])))

    reply_markup = InlineKeyboardMarkup([lst]) if lst else None
    bot.send_message(chat_id=update.effective_chat.id,
                     text="Imtihon ro'yxatidan birini tanlang 👇",
                     reply_markup=reply_markup)


def get_mark(percent):
    if percent < 40:
        return 2
    elif percent <= 60:
        return 3
    elif percent <= 86:
        return 4
    else:
        return 5


def sec2min(second):
    if not second:
        return ''
    if second <= 59:
        return '{} s'.format(second)
    minute = int(second / 60)
    sec = second - minute * 60
    hour = 0
    if minute > 59:
        hour = int(minute / 60)
        minute = minute - hour * 60
    if hour:
        return '{} soat {} min {} s'.format(hour, minute, sec)
    return '{} min {} s'.format(minute,sec)


def set_logger(logging, name):
    logging.basicConfig(filename='logs',
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d:%H:%M:%S',
                        level=logging.INFO)
    return logging.getLogger(name)


def make_menu_2d(rows: list, cols):
    button_list = []
    i = 0
    lst = []
    for row in rows:
        i += 1
        lst.append(row)
        if i % cols == 0:
            button_list.append(lst)
            lst = []

    if i % cols != 0:
        button_list.append(lst)
    return button_list


