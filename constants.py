#!/usr/bin/env python
# -*- coding: utf-8 -*-
LOCALES = {'uz': 'uz_UZ', 'ru': 'ru_RU', 'en': 'en_US'}

TIMEZONES = {
    'uz_UZ': 'Asia/Samarkand',
    'ru_RU': 'Europe/Moscow',
    'en_US': 'America/Los_Angeles'
}

# ICONS
ICON_CHECK = "✅"
ICON_WRITING = "✍️"
ICON_WIN = "🏆"
ICON_SELECTED = "👈"
ICON_RIGHT = "👍"
ICON_WRONG = "❌"
ICON_CLOCK = "⏰"
ICON_DISAPPOINTED = "😞"
ICON_SUCCESS = "😊"
ICON_WARNING = "❗️"
ICON_BELOW = "👇"
ICON_DATE = "📆"
ICON_EXAM = "🔴"
ICON_UZ = "🇺🇿"
ICON_RU = "🇷🇺"
ICON_US = "🇺🇸"
ICON_TADA = "🎉"
ICON_SETTING = "⚙️"
ICON_LEFT = "⬅️"
ICON_FILE = "📄"
ICON_DELETE = "❌"
ICON_ADD = "➕"
ICON_VIEW = "📋"
ICON_EDIT = "✏"
ICON_ATTACH = "📎"
ICON_PLAY = "▶️"
ICON_STOP = "⏹"

ICON_PHONE = "📱"
ICON_BOOKS = "📚"
ICON_RESULT = "📈"
ICON_TOTAL = "📊"
ICON_ANALYZE = "⚖"
ICON_INFO = "ℹ"
ICON_HANDS = "🤝"


BUTTON_REGISTRATION = 1
BUTTON_CONTACT = 2
BUTTON_EXAM = 3
BUTTON_TEST = 4
BUTTON_BACK = 5
BUTTON_BEGIN_EXAM = 6
BUTTON_INVITE_TO_EXAM = 7
BUTTON_TEST_INSERT = 8
BUTTON_TEST_DELETE = 9
BUTTON_TEST_INSERT_FILE = 10
BUTTON_TEST_VIEW = 11
BUTTON_TEST_GROUP_DELETE = 12
BUTTON_MONITOR_PLAY = 13
BUTTON_MONITOR_STOP = 14
BUTTON_MONITOR_BACK = 15
# learner buttons
BUTTON_TEST_BEGIN = 50  # "✅ Testni boshlash"
BUTTON_TEST_RESULT = 51  # "📈 Test natijalarim"
BUTTON_TEST_TOTAL = 52  # "📊 Test ballarim"
BUTTON_TEST_BY_THEME = 53  # "⚖️ Test tahlili"
BUTTON_BACK_TO_MAIN_MENU = 54  # "⬅️ Asosiy menyuga"
BUTTON_INFO = 55  # "ℹ️ Ma'lumot"
BUTTON_USER_SETTINGS = 56  # "⚙️ Sozlamalar"
BUTTON_USER_FIRST_NAME_EDIT = 57  # "✏️ Ismni o'zgartirish"
BUTTON_USER_LAST_NAME_EDIT = 58  # "✏️ Familiyani o'zgartirish"
BUTTON_USER_CLASS_EDIT = 59  # "✏️ Sinfni o'zgartirish"
BUTTON_DEMO = 60  # "🤝 Tanishib chiqish"
BUTTON_REGISTER = 61  # "✍️ Ro'yxatdan o'tish"

# inline function constants
KEY_NEW_TEST = 1
KEY_TEST_GROUP = 2
KEY_LANGUAGE = 3
KEY_TEST = 4
KEY_EXAM_MONITOR = 5
KEY_EXAM_SEND = 6
# exam monitor and exam send actions
ACTION_EMPTY = 0
ACTION_EXAM_LIST_VIEW = 1
ACTION_MONITOR = 2
ACTION_EXAM_CREATE = 3
ACTION_EXAM_SEND_LINK = 4
ACTION_EXAM_ANALYZE = 5
ACTION_BACK_TO_MONITOR = 6
ACTION_EXAM_ANALYZE_STUDENT = 7
ACTION_BACK_TO_EXAM_ANALYZE = 8

# KEY_TEST_GROUP_DELETE = 5
ACTION_VIEW = 1
ACTION_EDIT = 2
ACTION_DELETE = 3
ACTION_INSERT = 4
ACTION_INSERT_DOC = 5
# DOCX
DOC_INIT = 1
DOC_QUESTION = 2
DOC_ANSWERS = 3
DOC_FINISH = 4

######################### LEARNER #########################

# MENU_TEST_BEGIN = "✅ Testni boshlash"
# MENU_TEST_RESULT = "📈 Test natijalarim"
# MENU_TEST_TOTAL = "📊 Test ballarim"
# MENU_TEST_BY_THEME = "⚖️ Test tahlili"
# MENU_BACK_TO_MAIN_MENU = "⬅️ Asosiy menyuga"
# MENU_INFO = "ℹ️ Ma'lumot"
# MENU_USER_SETTINGS = "⚙️ Sozlamalar"
# MENU_USER_FIRST_NAME_EDIT = "✏️ Ismni o'zgartirish"
# MENU_USER_LAST_NAME_EDIT = "✏️ Familiyani o'zgartirish"
# MENU_USER_CLASS_EDIT = "✏️ Sinfni o'zgartirish"
# MENU_DEMO = "🤝 Tanishib chiqish"
# MENU_REGISTER = "✍️ Ro'yxatdan o'tish"

# DB status
EXAM_STATUS_BEGAN = 1
EXAM_STATUS_FINISHED = 2
EXAM_STATUS_NOT_BEGAN = 3
# keys
KEY_QUESTION = 1
KEY_EXAM_ANALYZE = 2
