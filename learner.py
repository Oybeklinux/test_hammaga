#!/usr/bin/python3.8
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Basic example for a bot that uses inline keyboards.
"""
import sys
import traceback

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, \
    KeyboardButton, ParseMode, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram.bot import Bot
from telegram.utils.helpers import mention_html

from db_manager import DBManager
from random import shuffle
from constants import *
import json
import logging
from general_functions import *

_ru = None
_uz = None
_us = None
token = None
db = None
bot = None


# exam ga from_date , to_date, duration qo'shiladi
# ikkita rejim bo'ladi:
# 1. Imtihon boshlanish va tugash payti va davomiyligi belgilanadi
# 2. Hech qanday ma'lumot kiritilmaydi, play/pause/stop tugmasi bilan boshqaradi
# Agar from_date , to_date, duration hammasi belgilanmagan bo'lsa, u holda status ga qaraladi


def start(update, context):
    global db, bot
    try:
        chat_id = update.effective_message.chat.id
        if not len(context.args):
            send_main_menu(update, context)
            return
        hash_value = context.args[0]
        context.user_data['exam_id'] = db.hash_to_value(hash_value)
        if db.if_learner_exist(chat_id):
            send_exam_status(update, context)
        else:
            send_languages(chat_id, bot)
    except Exception as ex:
        logger.error(ex)


def send_exam_status(update, context):
    chat_id = update.effective_message.chat.id
    if not db.set_user_entered_exam(chat_id, context.user_data['exam_id']):
        logger.error('Error in set_user_entered_exam')
        return
    row = db.get_exam_info(context.user_data['exam_id'])
    if not row:
        logger.warning('Xavola xato yuborilgan')
        return

    user_locale = get_user_locale(chat_id, context)
    if row['beg_date'] and row['end_date'] and row['duration']:
        text = tr(user_locale, _("O'qituvchi: {} {}\nImtihon nomi: {}\nHolati: {}")). \
            format(row['teacher_position'], row['teacher_name'], row['exam_name'],
                   row['beg_date'], row['end_date'], row['duration'])
    else:
        # agar beg_date bo'sh bo'lsa, demak imtihon hali boshlanmabdi
        text = tr(user_locale, _("O'qituvchi: *{} {}*\nImtihon nomi: *{}*\nImtihon boshlanish vaqti: " \
                                 "*{}*")).format(row['teacher_position'],
                                                 row["teacher_name"],
                                                 row['exam_name'],
                                                 row['beg_date'] if row['beg_date']
                                                 else tr(user_locale,
                                                         _("Imtihon boshlanmadi. Boshlansa, sizga habar keladi")))
    send_main_menu(update, context)
    bot.send_message(chat_id=chat_id,
                     text=text,
                     parse_mode=ParseMode.MARKDOWN)


def send_contact_menu(chat_id, context):
    global bot
    user_locale = get_user_locale(chat_id, context)
    reply_markup = ReplyKeyboardMarkup([[KeyboardButton(button(user_locale, BUTTON_CONTACT), request_contact=True)]],
                                       one_time_keyboard=True,
                                       resize_keyboard=True
                                       )
    bot.send_message(chat_id=chat_id,
                     text=ICON_BOOKS + " " + tr(user_locale, _("Test topshirish uchun ro'yxatdan o'ting. \nBuning "
                                       "uchun *{}* tugmasini bosing 👇")).format(button(user_locale, BUTTON_CONTACT)),
                     reply_markup=reply_markup,
                     parse_mode=ParseMode.MARKDOWN)


def send_test_sub_menu(update, context):
    try:
        chat_id = update.effective_chat.id
        user_locale = get_user_locale(chat_id, context)
        button_test_total = button(user_locale, BUTTON_TEST_TOTAL)
        button_test_by_theme = button(user_locale, BUTTON_TEST_BY_THEME)
        reply_markup = ReplyKeyboardMarkup([[KeyboardButton(button_test_total),
                                             KeyboardButton(button_test_by_theme)],
                                            [KeyboardButton(button(user_locale, BUTTON_BACK_TO_MAIN_MENU))]],
                                           resize_keyboard=True
                                           )

        bot.send_message(chat_id=chat_id,
                         text=tr(user_locale, _("*Asosiy bo'lim*:\n\n- Umumiy test ballarni ko'rmoqchi bo'lsangiz "
                                                "*{}* tugmasini bosing. \n   - Agar test imtihonidagi xatolaringiz "
                                                "ustida ishlamoqchi bo'lsangiz *{}* tugmasini bosing.\n")).
                         format(button_test_total, button_test_by_theme),
                         reply_markup=reply_markup,
                         parse_mode=ParseMode.MARKDOWN)
    except Exception as ex:
        logger.error(ex)


def send_main_menu(update, context):
    try:
        chat_id = update.effective_chat.id
        user_locale = get_user_locale(chat_id, context)
        button_test_begin = button(user_locale, BUTTON_TEST_BEGIN)
        button_test_result = button(user_locale, BUTTON_TEST_RESULT)
        reply_markup = ReplyKeyboardMarkup([[KeyboardButton(button_test_begin)],
                                            [KeyboardButton(button_test_result)]],
                                           resize_keyboard=True
                                           )
        bot.send_message(chat_id=chat_id,
                         text=tr(user_locale, _("*Test bo'limi*:\n\n️   - Test topshirish uchun *{}* tugmasini bosing. "
                                                "\n️   - Test natijalarini ko'rish uchun *{}* tugmasini bosing.\n")).
                         format(button_test_begin, button_test_result),
                         reply_markup=reply_markup,
                         parse_mode=ParseMode.MARKDOWN)
    except Exception as ex:
        logger.error(ex)


# def get_learner_score(chat_id, exam_id=None, test_id=None, context):
#     try:
#         user_locale = user_locale = get_user_locale(chat_id, context)
#         total, score = db.learner_score(chat_id=chat_id, exam_id=exam_id, test_id=test_id)
#         result = tr(user_locale, _('Sizning natijangiz\n'))
#         if total:
#             if score == 0:
#                 result += '*Hammasi xato*'
#             elif score == total:
#                 result += "*Hammasi to'g'ri*"
#             else:
#                 result += "{} savoldan *{} to'g'ri javob* berdingiz".format(total, score)
#         return result
#     except Exception as ex:
#         logger.error(ex)


def get_pupil_score(chat_id, exam_id, context):
    try:
        user_locale = get_user_locale(chat_id, context)
        total, score = db.pupil_score(chat_id=chat_id, exam_id=exam_id)
        result = tr(user_locale, _('Sizning natijangiz\n'))
        if total:
            if score == 0:
                result += tr(user_locale, _('*Hammasi xato*'))
            elif score == total:
                result += tr(user_locale, _("*Hammasi to'g'ri*"))
            else:
                result += tr(user_locale, _("{} savoldan *{} to'g'ri javob* berdingiz")).format(total, score)
        return result
    except Exception as ex:
        logger.error(ex)


def send_question(update, context, data=None, exam_id=None):
    global bot
    try:
        chat_id = update.effective_chat.id
        user_locale = get_user_locale(chat_id, context)
        exam_status = db.get_exam_status(exam_id)
        if not exam_status:
            logger.warning('exam_status is None, chat_id: ', chat_id)
            return
        if exam_status == EXAM_STATUS_FINISHED:
            bot.send_message(chat_id, text=tr(user_locale, _("Imtihon tugadi")))
            return
        elif exam_status == EXAM_STATUS_NOT_BEGAN:
            bot.send_message(chat_id, text=tr(user_locale, _("Imtihon boshlanmadi")))
            return

        if data:
            db.save_test_result(test_id=data['id'], chat_id=chat_id, exam_id=exam_id, user_answer=data['answer_index'])

        row = db.get_test(exam_id=exam_id, chat_id=chat_id)
        if not row:
            if data:
                bot.edit_message_text(chat_id=chat_id, message_id=update.effective_message.message_id,
                                      text=tr(user_locale, _("Imtihon tugadi. ")) + get_pupil_score(chat_id, exam_id, context),
                                      parse_mode=ParseMode.MARKDOWN)
            else:
                bot.send_message(chat_id=update.effective_chat.id,  # message_id=update.effective_message.message_id,
                                 text=tr(user_locale, _("Siz imtihonni topshirib bo'lgansiz")),
                                 parse_mode=ParseMode.MARKDOWN)
            return

        db.save_test_result(test_id=row['id'], chat_id=chat_id, exam_id=exam_id)

        # key = str(int(key) + 1)
        # savol: test_id-answer_index
        button_list = []
        answers = [row['answer1'], row['answer2'], row['answer3'], row['answer4'], row['answer5']]
        for answer in answers:
            if answer:
                callback_data = json.dumps(
                    {"key": KEY_QUESTION, "id": row['id'], "answer_index": answers.index(answer) + 1})
                button_list.append([InlineKeyboardButton(answer, callback_data=callback_data)])
        shuffle(button_list)
        reply_markup = InlineKeyboardMarkup(button_list)

        print('value', data)
        if data:
            bot.edit_message_text(chat_id=update.effective_chat.id, message_id=update.effective_message.message_id,
                                  text=row['question'],
                                  reply_markup=reply_markup)
        else:
            bot.send_message(chat_id=update.effective_chat.id,
                             text=row['question'],
                             reply_markup=reply_markup)
    except Exception as ex:
        logger.error(ex)


def register_learner(update, context):
    """
    :type context: object
    :type update: object
    :return True if success
    """
    global token, db, bot
    try:
        chat_id = update.effective_message.chat.id
        user_locale = get_user_locale(chat_id, context)
        language_id = db.get_language_id(context.user_data['locale'])
        if not db.save_learner(chat_id,
                               context.user_data['user']['first_name'],
                               context.user_data['user']['last_name'],
                               context.user_data['user']['username'],
                               context.user_data['user']['phone'],
                               language_id):
            logger.error('Error on saving learner')
            return False
        send_exam_status(update, context)
        del context.user_data['user']

        bot.send_message(chat_id=chat_id,
                         text=tr(user_locale, _("{}Siz ro'yxatdan muvaffaqiyalti o'tdingiz.{}\n Imtihonga tayyor "
                                                "bo\'lsangiz, *{}* tugmasini bosing")).
                         format(ICON_TADA, ICON_TADA, button(user_locale, BUTTON_TEST_BEGIN)),
                         parse_mode=ParseMode.MARKDOWN)
        return True
    except Exception as ex:
        logger.error(ex)
        return False


def handle_inline_keyboard(update, context):
    global token, db, bot
    try:
        query = update.callback_query
        data = query.data
        data = json.loads(data)
        print("answer: ", data)
        chat_id = update.effective_chat.id
        key = data['key']
        if key == KEY_LANGUAGE:  # "language":
            context.user_data['locale'] = data['locale']
            send_contact_menu(chat_id, context)
            return
        elif key == KEY_QUESTION:
            send_question(update, context, data, context.user_data['exam_id'])
        elif key == KEY_EXAM_ANALYZE:
            # vaqtincha yopib turaman
            row = db.is_test_analyze_open(data['exam_id'])
            if not row:
                bot.send_message(chat_id=update.effective_chat.id,
                                 text="Bu imtihonga tegishli test tahlillarini ko'rishga hozircha  "
                                      "ruhsat berilmagan")
                return
            callback = lambda t: bot.send_message(chat_id=chat_id,
                                                  text=t,
                                                  parse_mode=ParseMode.MARKDOWN)
            send_student_test_analyze(db, context, chat_id, data['exam_id'], callback, query['id'], learner_id=chat_id)
    except Exception as ex:
        logger.error(ex)


def send_student_test_analyze(db_: DBManager, context, chat_id, exam_id, callback=None, query_id=None, learner_id=None):
    rows = db_.get_test_analyze(chat_id=learner_id, exam_id=exam_id)
    if not rows:
        context.bot.answer_callback_query(callback_query_id=query_id, text = "Ma'lumot topilmadi", show_alert=True)

        return
    i = 0
    try:
        question_report = ''
        right_answers = 0
        duration = 0
        user_locale = get_user_locale(chat_id, context)
        for row in rows:
            i += 1
            right = row['user_answer'] == row['right_answer']
            right_answers += 1 if right else 0
            duration += row['duration'] if row['duration'] else 0
            question_report += "\n\n*Savol {}*                      {} {}\n{}\n   {} {}{} {}".format(
                i,
                ICON_CLOCK,
                sec2min(row['duration']) if row['duration'] else (row['began'] if row['began'] else ''),
                row['question'],
                row['user_answer'] if row['user_answer'] else '',
                ICON_RIGHT if right else ICON_WRONG,
                "\n   " + row['right_answer'] if not right else '',
                ICON_CHECK if not right else '')
        mark = int(right_answers * 100 / len(rows))
        print("Report: ", chat_id)
        print("locale ", "\n\n*Savol {}*                      {} {}\n{}\n   {} {}{} {}")
        conclusion = "\n\n*Savollar soni:* {}\n*To'g'ri javoblar soni:* {}\n" \
                                       "*Natija:* {} %\n*Umumiy ketgan vaqt:* {} ". \
            format(len(rows), right_answers, mark, sec2min(duration) if duration else '0')
        text = question_report + conclusion

        callback(text)

    except Exception as ex:
        print('==== ERROR ====')
        print(ex)


def help(update, context):
    chat_id = update.effective_chat.id
    user_locale = get_user_locale(chat_id, context)
    update.message.reply_text(tr(user_locale, _("Use /start to test this bot.")))


def error(update, context):
    # add all the dev user_ids in this list. You can also add ids of channels or groups.
    devs = [57018741]
    # we want to notify the user of this problem. This will always work, but not notify users if the update is an
    # callback or inline query, or a poll update. In case you want this, keep in mind that sending the message
    # could fail
    if update and update.effective_message:
        text = "Hey. I'm sorry to inform you that an error happened while I tried to handle your update. " \
               "My developer(s) will be notified."
        update.effective_message.reply_text(text)
    # This traceback is created with accessing the traceback object from the sys.exc_info, which is returned as the
    # third value of the returned tuple. Then we use the traceback.format_tb to get the traceback as a string, which
    # for a weird reason separates the line breaks in a list, but keeps the linebreaks itself. So just joining an
    # empty string works fine.
    trace = "".join(traceback.format_tb(sys.exc_info()[2]))
    # lets try to get as much information from the telegram update as possible
    payload = ""
    # normally, we always have an user. If not, its either a channel or a poll update.
    if update and update.effective_user:
        payload += f' with the user {mention_html(update.effective_user.id, update.effective_user.first_name)}'
    # there are more situations when you don't get a chat
    if update.effective_chat:
        payload += f' within the chat <i>{update.effective_chat.title}</i>'
        if update.effective_chat.username:
            payload += f' (@{update.effective_chat.username})'
    # but only one where you have an empty payload by now: A poll (buuuh)
    if update.poll:
        payload += f' with the poll id {update.poll.id}.'
    # lets put this in a "well" formatted text
    text = f"Hey.\n The error <code>{context.error}</code> happened{payload}. The full traceback:\n\n<code>{trace}" \
           f"</code>"
    # and send it to the dev(s)
    for dev_id in devs:
        context.bot.send_message(dev_id, text, parse_mode=ParseMode.HTML)
    # we raise the error again, so the logger module catches it. If you don't use the logger module, use it.
    raise



def handle_poll(update: Updater, context):
    print(update)


def send_exam_marks(chat_id, context):
    try:
        rows = db.get_exam_marks(chat_id)
        result = ''
        user_locale = get_user_locale(chat_id, context)
        if not rows:
            bot.send_message(chat_id=chat_id,
                             text=tr(user_locale, _("Siz hali imtihon topshirmagansiz")),
                             parse_mode=ParseMode.MARKDOWN)
            return

        for row in rows:
            result += tr(user_locale, _("\n\nImtihon sanasi: *{}*\nImtihon nomi: *{}* \nSavollar soni: {}\n" \
                                        "To'g'ri javoblar soni: *{}*\nTestga ketgan vaqt: *{}*\nNatija: *{} %*")).\
                format(row['beg_date'],
                       row['name'],
                       row['total'],
                       row['right_answers'],
                       sec2min(row['duration']),
                       int(row['right_answers'] * 100 / row['total']))
        bot.send_message(chat_id=chat_id, text=result, parse_mode=ParseMode.MARKDOWN)
    except Exception as ex:
        logger.error(ex)


def handle_text(update: Updater, context):
    global db, bot
    try:
        chat_id = update.effective_chat.id
        user_locale = get_user_locale(chat_id, context)
        if update.message.text == button(user_locale, BUTTON_TEST_BEGIN):
            if 'exam_id' not in context.user_data:
                bot.send_message(chat_id=chat_id,
                                 text=tr(user_locale, _("Imtihon topshirish uchun muallim sizni taklif qilishi kerak")))
                return
            send_question(update, context, exam_id=context.user_data['exam_id'])
        elif update.message.text == button(user_locale, BUTTON_TEST_RESULT):
            send_test_sub_menu(update, context)
        elif update.message.text == button(user_locale, BUTTON_BACK_TO_MAIN_MENU):
            send_main_menu(update, context)
        elif update.message.text == button(user_locale, BUTTON_TEST_TOTAL):
            send_exam_marks(chat_id, context)
        elif update.message.text == button(user_locale, BUTTON_TEST_BY_THEME):
            print("Test tahlili: ", chat_id)
            send_exam_list(update, context)
        elif 'key' in context.user_data:
            if 'name' == context.user_data['key']:
                # db.save_learner(chat_id=update.message.chat.id, name=update.message.text)
                context.user_data['user']['first_name'] = update.message.text
                bot.send_message(chat_id=update.effective_chat.id,
                                 text=tr(user_locale, _("Familiyangizni kiriting")))
                context.user_data['key'] = "last_name"
            elif 'last_name' == context.user_data['key']:
                context.user_data['user']['last_name'] = update.message.text
                if not register_learner(update, context):
                    logger.error('error in registering')
    except Exception as ex:
        logger.error(ex)


def send_exam_list(update: Updater, context):
    global db, bot
    chat_id = update.effective_chat.id
    user_locale = get_user_locale(chat_id, context)
    rows = db.learner_exam_list(chat_id=chat_id)
    lst = []
    if not rows:
        user_locale = get_user_locale(chat_id, context)
        bot.send_message(chat_id=update.effective_chat.id,
                         text=tr(user_locale, _("Topshirilgan imtihon mavjud emas")))
        return
    for row in rows:
        print(row['name'])
        callback_data = json.dumps({"key": KEY_EXAM_ANALYZE, "exam_id": row['id']})

        lst.append([InlineKeyboardButton(text='{} {}'.format(row['name'], row['date']),
                                         callback_data=callback_data)])

    reply_markup = InlineKeyboardMarkup(lst) if lst else None
    bot.send_message(chat_id=update.effective_chat.id,
                     text=tr(user_locale, _("Imtihon ro'yxatidan birini tanlang 👇")),
                     reply_markup=reply_markup)


def get_contact(update: Updater, context):
    global db, bot
    try:
        contact = update.effective_message.contact
        # phone = contact.phone_number
        # username = update.effective_user.username
        # db.save_learner(chat_id=update.message.chat.id, phone=phone, user_name=username)
        context.user_data['user'] = {}
        context.user_data['user']['phone'] = contact.phone_number
        context.user_data['user']['username'] = update.effective_user.username

        chat_id = update.effective_chat.id
        user_locale = get_user_locale(chat_id, context)
        bot.send_message(chat_id=chat_id,
                         text=tr(user_locale, _("Ismingizni kiriting"))
                         )
        context.user_data['key'] = "name"
    except Exception as ex:
        logger.error(ex)


def button(user_locale, button_constant):
    if button_constant == BUTTON_CONTACT:
        value = " " + tr(user_locale, _("Kontaktni jo'natish"))
        return ICON_PHONE + value
    elif button_constant == BUTTON_TEST_BEGIN:
        value = " " + tr(user_locale, _("Testni boshlash"))
        return ICON_CHECK + value
    elif button_constant == BUTTON_TEST_RESULT:
        value = " " + tr(user_locale, _("Test natijalarim"))
        return ICON_RESULT + value
    elif button_constant == BUTTON_TEST_TOTAL:
        value = " " + tr(user_locale, _("Test ballarim"))
        return ICON_TOTAL + value
    elif button_constant == BUTTON_TEST_BY_THEME:
        value = " " + tr(user_locale, _("Test tahlili"))
        return ICON_ANALYZE + value
    elif button_constant == BUTTON_BACK_TO_MAIN_MENU:
        value = " " + tr(user_locale, _("Asosiy menyuga"))
        return ICON_LEFT + value
    elif button_constant == BUTTON_INFO:
        value = " " + tr(user_locale, _("Ma'lumot"))
        return ICON_INFO + value
    elif button_constant == BUTTON_USER_SETTINGS:
        value = " " + tr(user_locale, _("Sozlamalar"))
        return ICON_SETTING + value
    elif button_constant == BUTTON_USER_FIRST_NAME_EDIT:
        value = " " + tr(user_locale, _("Ismni o'zgartirish"))
        return ICON_EDIT + value
    elif button_constant == BUTTON_USER_LAST_NAME_EDIT:
        value = " " + tr(user_locale, _("Familiyani o'zgartirish"))
        return ICON_EDIT + value
    elif button_constant == BUTTON_REGISTER:
        value = " " + tr(user_locale, _("Ro'yxatdan o'tish"))
        return ICON_WRITING + value


def get_user_locale(chat_id, context):
    global db
    if 'locale' in context.user_data.keys():
        return context.user_data['locale']
    else:
        return db.get_locale(chat_id)


def tr(user_locale, text):
    global _ru, _uz, _us

    if user_locale == 'uz':
        return _uz(text)
    elif user_locale == "ru":
        return _ru(text)
    else:
        return _us(text)


def _(value):
    """
    Tarjima qilinishi uchun
    :param value - string
    """
    return value


def main():
    global token, bot, _ru, _uz, _us
    _ru, _uz, _us = init_locales("learner")

    logger.info("starting the bot")
    updater = Updater(token=token, use_context=True)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CallbackQueryHandler(handle_inline_keyboard))
    updater.dispatcher.add_handler(MessageHandler(Filters.contact, get_contact))
    updater.dispatcher.add_handler(MessageHandler(Filters.text, handle_text))
    updater.dispatcher.add_handler(MessageHandler(Filters.poll, handle_poll))
    updater.dispatcher.add_handler(CommandHandler('help', help))
    updater.dispatcher.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    bot = Bot(token=token)
    # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT
    updater.idle()


if __name__ == '__main__':
    logger = set_logger(logging, 'Pupil')

    with open('config.json', 'r') as file:
        config = json.load(file)

    logger.info("reading database")
    token = config['student_token']
    db = DBManager(config['db'])
    main()
