import sqlite3
from sqlite3 import Error, Row
from random import randrange
import datetime
import xml.etree.ElementTree as ET
from general_functions import set_logger
import logging
from constants import EXAM_STATUS_BEGAN, EXAM_STATUS_FINISHED, EXAM_STATUS_NOT_BEGAN


class DBManager:

    def __init__(self, dbname=None):
        self.logger = set_logger(logging, 'DBManager')
        self.dbname = dbname
        self.conn = None
        self.open_connection()
        self.pupil_list = dict()

    def open_connection(self):
        try:
            self.conn = sqlite3.connect(self.dbname, check_same_thread=False)
            self.conn.row_factory = Row
            print(sqlite3.version)
            self.migrate()
        except Error as e:
            print(e)

    def close_connection(self):
        self.conn.close()

    # TEACHER FUNCTIONS
    # test group
    def get_test_group(self, chat_id):
        sql = """
            SELECT id, name, created
            FROM test_group
            WHERE teacher_chat_id=?
            ORDER BY created DESC"""
        try:
            cur = self.conn.cursor()
            cur.execute(sql, (chat_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)
            return None

    def insert_test_group(self, chat_id):
        cur = self.conn.cursor()
        sql = '''INSERT INTO test_group(name, created, teacher_chat_id)
                    VALUES(?, datetime(CURRENT_TIMESTAMP, 'localtime'), ?)'''
        try:
            name = datetime.datetime.strftime(datetime.datetime.now(), "%d.%m.%Y %H:%M")
            cur.execute(sql, (name, chat_id))
            self.conn.commit()
            return cur.lastrowid, name
        except Exception as ex:
            self.logger.error(ex)
            return None, None

    def insert_exam(self, name, test_group_id, chat_id):
        cur = self.conn.cursor()
        sql = '''INSERT INTO exam(name, created, test_group_id, teacher_chat_id)
                            VALUES(?, datetime(CURRENT_TIMESTAMP, 'localtime'), ?, ?)'''
        try:
            cur.execute(sql, (name, test_group_id, chat_id))
            self.conn.commit()
            return cur.lastrowid
        except Exception as ex:
            self.logger.error(ex)
            return None, None

    def save_hash_value(self, id, hash_value):
        cur = self.conn.cursor()
        sql = '''INSERT INTO hash(id, value)
                            VALUES(?, ?)'''
        try:
            cur.execute(sql, (id, hash_value))
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def register_teacher(self, chat_id, locale, value):
        """
        Create a new pupil into the pupil table
        :return: last row id, returns -1 when last_name and first_name are filled
        """
        args = (chat_id,
                value['first_name'],
                value['last_name'],
                value['field'],
                locale
                )

        sql = ''' REPLACE INTO teacher(chat_id, first_name, last_name, field, created, language_id)
                          VALUES(?,?,?,?,datetime(CURRENT_TIMESTAMP, 'localtime'),(select id from language where short_name=?))'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, args)
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def get_locale(self, chat_id):
        """
        get user locale - language
        """
        sql = """
                SELECT short_name from language where id = (SELECT language_id FROM learner where chat_id=?)"""
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (chat_id, ))
            row = cur.fetchone()
            return row[0] if row else None
        except Error as e:
            print(e)

    def get_user_locale(self, chat_id):
        """
        get user locale - language
        """
        sql = """
                SELECT short_name from language where id = (SELECT language_id FROM teacher where chat_id=?)"""
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (chat_id, ))
            row = cur.fetchone()
            return row[0] if row else None
        except Error as e:
            print(e)

    def if_teacher_exist(self, chat_id):
        """
        :param chat_id: chat id
        :return True if exists:
        """
        cur = self.conn.cursor()
        try:
            cur.execute("SELECT count(*) total FROM teacher WHERE chat_id=?", (chat_id, ))
            row = cur.fetchone()
            return int(row[0]) > 0
        except Exception as ex:
            self.logger.error(ex)

    def if_learner_exist(self, chat_id):
        """
        :param chat_id: chat id
        :return True if exists:
        """
        cur = self.conn.cursor()
        try:
            cur.execute("SELECT count(*) total FROM learner WHERE chat_id=?", (chat_id, ))
            row = cur.fetchone()
            return int(row[0]) > 0
        except Exception as ex:
            self.logger.error(ex)
            return False

    def get_exam_status(self, exam_id):
        """
        :param exam_id
        :return status of exam
        """
        cur = self.conn.cursor()
        try:
            cur.execute("SELECT end_date, beg_date FROM exam WHERE id=?", (exam_id,))
            row = cur.fetchone()
            if row['end_date']:
                return EXAM_STATUS_FINISHED
            elif row['beg_date']:
                return EXAM_STATUS_BEGAN
            else:
                return EXAM_STATUS_NOT_BEGAN
        except Exception as ex:
            self.logger.error(ex)
            return None

    def hash_to_value(self, hash):
        """
        :param hash: hash
        :return value of hash
        """
        cur = self.conn.cursor()
        try:
            cur.execute("SELECT id FROM hash WHERE value=?", (hash,))
            row = cur.fetchone()
            return row['id']
        except Exception as ex:
            self.logger.error(ex)
            return None

    def get_pupil_class(self):
        """
        Query all rows in the tasks table
        :return list of pupil classes:
        """
        sql = """
        SELECT DISTINCT p.id, p.name, datetime(CURRENT_TIMESTAMP, 'localtime') > test_begin [exam_exist]
        FROM pupil_class p
        LEFT JOIN timetable t on t.pupil_class_id=p.id"""
        cur = self.conn.cursor()
        try:
            cur.execute(sql)
            rows = cur.fetchall()
        except Error as e:
            print(e)
        return rows

    def get_future_exams(self, chat_id):
        sql ="""
        SELECT theme_id, strftime("%d:%m:%Y",test_begin) [test_date],
            strftime("%H:%M",test_begin) [test_begin_time],
            strftime("%H:%M",test_end) [test_end_time]
        FROM timetable 
        WHERE datetime(CURRENT_TIMESTAMP, 'localtime') < test_begin 
            and pupil_class_id=(SELECT pupil_class_id from pupil where chat_id=?)"""
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (chat_id,))
        except Exception as ex:
            self.logger.error(ex)
        return cur.fetchone()

    def if_user_able_pass_exam(self, chat_id):
        """
        Query if user is able to take exam
        chat_id chat_id of pupil
        :return theme id of test
        """
        sql = """
            SELECT count(*) [total],theme_id
            FROM test 
            WHERE theme_id =
                (SELECT max(theme_id)
                FROM timetable 
                WHERE datetime(CURRENT_TIMESTAMP, 'localtime') BETWEEN test_begin and test_end
                    and pupil_class_id=(SELECT pupil_class_id from pupil where chat_id=?))"""
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (chat_id,))
            row = cur.fetchone()
            if row and row['total'] > 0:
                return row['theme_id']
            else:
                return None
        except Exception as ex:
            self.logger.error(ex)

    def save_tests(self, test_group_id, questions, right_answers):
        i = 0
        cur = self.conn.cursor()
        sql = '''REPLACE INTO test(test_group_id, question, answer1,answer2,answer3,answer4,answer5, right_answer, test_type_id)
                                        VALUES(?,?,?,?,?,?,?,?,1)'''
        try:
            for item in questions:
                question = item['question']
                answers = item['answer']
                args = (
                    test_group_id,
                    question,
                    answers[0],
                    answers[1],
                    answers[2] if len(answers) > 2 else None,
                    answers[3] if len(answers) > 3 else None,
                    answers[4] if len(answers) > 4 else None,
                    right_answers[i]
                )
                cur.execute(sql, args)
                i += 1
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def save_test_result(self, test_id, chat_id, exam_id, user_answer=None):
        """
        Query saves test which is given to pupil but is not answered yet
        :param test_id - test id
        :param chat_id - id of learner
        :param exam_id - id of exam
        :param user_answer - user answer of test
        :return list of tests
        """
        try:
            cur = self.conn.cursor()
            if not test_id or not chat_id:
                print("error: empty test_id or chat_id")
                return
            if not user_answer:
                sql = '''REPLACE INTO test_results(test_id, learner_id, exam_id, created)
                        VALUES(?,?,?,datetime(CURRENT_TIMESTAMP, 'localtime'))'''
                cur.execute(sql, (test_id, chat_id, exam_id, ))
            else:
                sql = """
                    UPDATE test_results SET user_answer=?, finished=datetime(CURRENT_TIMESTAMP, 'localtime')
                    WHERE test_id=? and learner_id=? and exam_id=?
                """
                cur.execute(sql, (user_answer, test_id, chat_id, exam_id))
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def learner_score(self, chat_id, exam_id=None):
        """
        Query pupils test score by theme
        :param chat_id - chat_id of pupil
        :param theme_id - theme of test
        :return list of tests results if exist otherwise None
        """
        cur = self.conn.cursor()
        try:
            sql = '''
                SELECT count(*) [total], sum(t.right_answer=r.user_answer) [score]
                FROM test t
                LEFT JOIN (
                    SELECT * FROM test_results 
                    WHERE test_id in (SELECT id FROM test WHERE theme_id=?) 
                        AND pupil_id = (SELECT id FROM pupil WHERE chat_id=?)) r ON t.id=r.test_id
                WHERE t.theme_id=?'''
            cur.execute(sql, (theme_id, chat_id, theme_id,))
            row = cur.fetchone()
            return (row['total'], row['score']) if row else (None, None)
        except Exception as ex:
            self.logger.error(ex)

    def pupil_test_report(self, chat_id, theme_id):
        """
        Query pupils test result by theme
        :param chat_id - chat_id of pupil
        :param theme_id - theme of test
        :return list of tests results if exist otherwise None
        """
        sql = '''
            SELECT t.id, t.right_answer, r.created, r.user_answer, t.right_answer=r.user_answer
            FROM test t
            LEFT JOIN (
                SELECT * FROM test_results 
                WHERE test_id in (SELECT id FROM test WHERE theme_id=?) 
                    AND pupil_id = (SELECT id FROM pupil WHERE chat_id=?)) r ON t.id=r.test_id
            WHERE t.theme_id=?
            ORDER BY t.id'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (theme_id, chat_id,))
            rows = cur.fetchall()
            return rows if rows else None
        except Exception as ex:
            self.logger.error(ex)

    def pupil_score(self, chat_id, exam_id):
        """
        Query pupils test score by theme
        :param chat_id - chat_id of pupil
        :param exam_id - exam of user
        :return list of tests results if exist otherwise None
        """
        sql = None
        cur = self.conn.cursor()
        try:
            sql = '''
                SELECT count(*) [total], sum(t.right_answer=r.user_answer) [score]--,exam_id,test_group_id
                FROM test t
                LEFT JOIN test_results r on t.id = r.test_id
                WHERE r.learner_id=? and r.exam_id=?'''
            cur.execute(sql, (chat_id, exam_id,))

            row = cur.fetchone()
            return (row['total'], row['score']) if row else (None, None)
        except Exception as ex:
            self.logger.error(ex)

    def get_test(self, exam_id, chat_id):
        """
        Query all test related to exam_id
        :param chat_id - chat_id of learner
        :param exam_id - exam
        :return list of tests if exist otherwise None
        """
        sql = """
            SELECT * 
            FROM test
            WHERE test_group_id= (SELECT test_group_id FROM exam WHERE id=?) 
            AND id not in 
                (SELECT test_id
                FROM test_results
                WHERE learner_id=? AND exam_id=? AND user_answer IS NOT NULL)"""

        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id, chat_id, exam_id,))
            rows = cur.fetchall()
            if rows:
                random_number = randrange(0, len(rows))
                return rows[random_number]
            else:
                return None
        except Exception as ex:
            self.logger.error(ex)

    def get_link_param(self, chat_id, hash_value):
        """
        Query teacher name and exam name
        :param chat_id
        :param hash_value
        :return exam info
        """
        sql = """SELECT e.name [exam], coalesce(last_name,'') || ' ' || coalesce(first_name,'') [teacher]
                FROM exam e
                LEFT JOIN teacher t ON t.chat_id=e.teacher_chat_id
                WHERE id = (SELECT id from hash where value=?) and e.teacher_chat_id=?
                """
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (hash_value, chat_id))
            return cur.fetchone()
        except Exception as ex:
            self.logger.error(ex)

    def get_students(self, exam_id):
        """
        Query all students
        :param exam_id - exam
        :return list of students chat_id
        """
        sql = """SELECT DISTINCT learner_id [chat_id]
            FROM test_results
            WHERE exam_id = ?"""
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)

    def set_user_entered_exam(self, chat_id, exam_id):
        """
        Set user entered the exam
        :param chat_id - chat_id of learner
        :param exam_id - exam
        :return list of tests if exist otherwise None
        """
        sql = """
            REPLACE INTO test_results(created, learner_id, exam_id)
            VALUES(datetime(CURRENT_TIMESTAMP, 'localtime'), ?, ?)
            """

        cur = self.conn.cursor()
        try:
            ret = cur.execute(sql, (chat_id, exam_id, ))
            self.conn.commit()
            return ret and ret.rowcount > 0
        except Exception as ex:
            self.logger.error(ex)

    def set_exam_state(self, exam_id, state):
        """
        Set exam state began/finish
        :param state - state of exam
        :param exam_id - exam
        :return True if success
        """
        sql_begin = """
            UPDATE exam SET beg_date=?, end_date=NULL
            WHERE id=?
            """
        sql_end = """
            UPDATE exam SET end_date=? 
            WHERE id=?
            """
        cur = self.conn.cursor()
        try:
            if state == EXAM_STATUS_BEGAN:
                ret = cur.execute(sql_begin, (datetime.datetime.now(), exam_id,))
            else:
                ret = cur.execute(sql_end, (datetime.datetime.now(), exam_id,))
            self.conn.commit()
            return ret and ret.rowcount > 0
        except Exception as ex:
            self.logger.error(ex)

    def is_test_used(self, test_id, test_group_id):
        """
        Check if test is used by pupil in exam
        :param test_id - test to be deleted
        :param test_group_id - tests to be deleted by
        :return True if used otherwise False
        """
        cur = self.conn.cursor()
        try:
            if test_id:
                cur.execute("SELECT * FROM test_results WHERE test_id=?", (test_id,))
            else:
                cur.execute("SELECT * FROM test_results WHERE test_id in (select id from test where test_group_id=?)", (test_group_id,))
            row = cur.fetchone()
            return True if row else False
        except Exception as ex:
            self.logger.error(ex)

    def delete_test(self, test_id, test_group_id):
        """
        Delete test
        :param test_id - test to be deleted
        :return True if success otherwise False
        """
        try:
            cur = self.conn.cursor()
            if test_id:
                cur.execute('DELETE FROM test WHERE id  = ?', (test_id,))
            else:
                cur.execute('DELETE FROM test WHERE id in (select id from test where test_group_id=?)', (test_group_id,))
                cur.execute('DELETE FROM test_group WHERE id = ?', (test_group_id,))
                cur.execute('DELETE FROM exam WHERE test_group_id = ?', (test_group_id,))
            self.conn.commit()

            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def get_tests(self, test_group_id):
        """
        Query all test between duration time by theme related to user
        :param theme_id - theme of test
        :return list of tests if exist otherwise None
        """
        sql = """
            SELECT * 
            FROM test
            WHERE test_group_id= ?"""
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (test_group_id, ))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)

    def get_language_id(self, name):
        """
        Get language id
        :return: language id
        """
        sql = '''SELECT id FROM language WHERE short_name=?'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (name,))
            row = cur.fetchone()
            return row['id'] if row else None
        except Exception as ex:
            self.logger.error(ex)
            return None

    def save_learner(self, chat_id, first_name, last_name, username, phone, language_id):
        """
        Create a new learner
        :rtype: object
        :return: True if success
        """
        sql = ''' INSERT INTO learner(chat_id, first_name, last_name, username, created, phone, language_id)
                  VALUES(?,?,?,?,datetime(CURRENT_TIMESTAMP, 'localtime'),?,?)'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (chat_id, first_name, last_name, username, phone, language_id))
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def create_table(self, create_table_sql):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        :return:
        """
        try:
            c = self.conn.cursor()
            c.execute(create_table_sql)
        except Error as e:
            print(e)

    def save_settings(self, key, value):
        """
        :param key - version, color, ..
        :param value - value of key
        """
        cur = self.conn.cursor()
        sql = '''REPLACE INTO settings(key, value) VALUES(?,?)'''
        try:
            cur.execute(sql, (key, value,))
            self.conn.commit()
        except Exception as ex:
            self.logger.error(ex)
            self.conn.rollback()

    def has_exam_changes(self, datetm, exam_id):
        """
        check if there is changes (exam, registration) since datatm related to class_id
        :param datetm - last time of changes
        :param exam_id
        :return True if changes exist otherwise False
        """
        sql = """
            SELECT learner_id [user]
            FROM test_results 
            WHERE JulianDay(created) > JulianDay(?) AND exam_id=?"""
        try:
            cur = self.conn.cursor()
            cur.execute(sql, (datetm, exam_id,))
            row = cur.fetchone()

            return True if row and row['user'] else False
        except Exception as ex:
            self.logger.error(ex)
            print(ex)


    def save_theme(self, name, class_type_id, deadline=None):
        """
        Query saves newly created test
        :param name - theme name
        :param deadline - after that time user can see answers
        :param class_type_id - class type of pupil
        :return True if success otherwise False
        """
        try:
            cur = self.conn.cursor()
            sql = '''REPLACE INTO theme(name, class_type_id, created)
                                VALUES(?,?,datetime(CURRENT_TIMESTAMP, 'localtime'))'''
            cur.execute(sql, (name, class_type_id))
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def check_if_test_exist(self, theme_id):
        """
        check if there are test related to theme
        :param theme_id  - theme
        :return True if exist otherwise False
        """
        sql = """SELECT * FROM test
                    WHERE theme_id  = ?"""
        try:
            cur = self.conn.cursor()
            cur.execute(sql, (theme_id, ))
            return True if cur.fetchone() else False
        except Exception as ex:
            self.logger.error(ex)
            print(ex)

    def delete_theme(self, theme_id):
        """
        Delete theme
        :param theme_id - theme to be deleted
        :return True if success otherwise False
        """
        try:
            cur = self.conn.cursor()
            sql = 'DELETE FROM theme WHERE id  = ?'
            cur.execute(sql, (theme_id, ))
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def get_theme(self, class_type_id):
        """
        get theme of exams related to class
        :param class_type_id  - pupil class
        :return exam themes
        """
        sql = """
            SELECT id, name, strftime("%d.%m.%Y %H:%M",deadline) [deadline]
            FROM theme
            WHERE class_type_id=?
            ORDER BY created DESC"""
        try:
            cur = self.conn.cursor()
            cur.execute(sql, (class_type_id,))
        except Exception as ex:
            self.logger.error(ex)
        return cur.fetchall()

    def get_reg_pupils(self, date_time):
        """
        get pupil who is just was registered
        :param date_time  - time of registration
        :return just registered pupils
        """
        sql = """
        SELECT p.last_name, p.first_name, p.phone, c.name [class_name]
        FROM pupil p
        LEFT JOIN pupil_class c ON c.id=p.pupil_class_id
        WHERE JulianDay(p.created) > JulianDay(?)"""
        try:
            cur = self.conn.cursor()
            cur.execute(sql, (date_time,))
        except Exception as ex:
            self.logger.error(ex)
        return cur.fetchall()

    def save_exam_time(self, theme_id, class_id, test_begin, test_end):
        cur = self.conn.cursor()
        sql = '''INSERT INTO timetable(theme_id, pupil_class_id, test_begin, test_end) 
                VALUES(?,?,?,?)'''
        try:
            cur.execute(sql, (theme_id, class_id, test_begin, test_end))
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def delete_exam_time(self, timetable_id):
        """
        check if timetable can be deleted
        :param timetable_id  - timetable_id
        :return True if deleted otherwise False
        """
        try:
            cur = self.conn.cursor()
            sql = 'DELETE FROM timetable WHERE id=?'
            cur.execute(sql, (timetable_id,))
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False

    def exam_time_deletable(self, timetable_id):
        """
        check if timetable can be deleted
        :param timetable_id  - timetable_id
        :return True if deletable otherwise False
        """
        sql = """SELECT count(*) [total]
                FROM test_results t, (SELECT * FROM timetable WHERE id=?) b
                LEFT JOIN pupil p ON t.pupil_id=p.id
                LEFT JOIN test s ON t.test_id = s.id
                WHERE s.theme_id = b.theme_id and p.pupil_class_id=b.pupil_class_id"""
        try:
            cur = self.conn.cursor()
            cur.execute(sql, (timetable_id,))
            row = cur.fetchone()
            return False if row['total'] > 0 else True
        except Exception as ex:
            self.logger.error(ex)

    def get_exam_info(self, exam_id):
        """
        get all exam_id exam
        :return exam info
        """
        sql = '''SELECT  e.name [exam_name], strftime("%d.%m.%Y",e.beg_date) [beg_date], strftime("%d.%m.%Y",e.end_date) [end_date], e.duration, e.status, t.last_name || ' ' || t.first_name [teacher_name], t.field [teacher_position]
                FROM exam e
                LEFT JOIN teacher t on t.chat_id = e.teacher_chat_id
                WHERE e.id=?'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id, ))
            return cur.fetchone()
        except Exception as ex:
            self.logger.error(ex)

    def get_timetable(self, class_type_id, class_id):
        """
        get all exams related to class
        :param class_id  - class id
        :param class_type_id  - class type id
        :return user marks of all exam
        """

        sql = '''SELECT t.id [timetable_id],h.id [theme_id], h.name,strftime("%d.%m.%Y",t.test_begin) [date],strftime("%H:%M",t.test_begin) [test_begin], strftime("%H:%M",t.test_end) [test_end]
                FROM theme h
                LEFT JOIN timetable t ON t.pupil_class_id=? and t.theme_id=h.id 
                WHERE class_type_id=?
                ORDER BY h.created DESC'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (class_id, class_type_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)

    def get_exam_marks(self, chat_id):
        """
        :param chat_id  - chat id of user
        :return user marks of all exam
        """
        sql = '''
            SELECT e.id,e.name, strftime("%d.%m.%Y",e.created) [beg_date], sum(t.right_answer == r.user_answer) [right_answers], count(*) [total],
            sum(Cast ((JulianDay(r.finished) - JulianDay(r.created)) * 24 * 60 * 60 As Integer)) [duration]
            , sum(learner_id) [qatnashdi]
            FROM exam e
            LEFT JOIN test t ON t.test_group_id = e.test_group_id
            LEFT JOIN test_results r ON t.id = r.test_id and  r.learner_id=? and r.test_id --r.exam_id=8 and
            GROUP BY e.id
            HAVING qatnashdi is not null
        '''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (chat_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)

    def is_test_analyze_open(self, exam_id):
        """
        Query all rows in the tasks table
        :param exam_id  - exam
        :return True if user can view his/her test results
        """
        sql = '''
        SELECT *
        FROM exam
        WHERE id = ? and end_date is not null'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id, ))
            return cur.fetchone()
        except Exception as ex:
            self.logger.error(ex)

    def migrate(self):
        db_version = int(self.db_version())
        tree = ET.parse('xml/migration.xml')
        root = tree.getroot()
        cur = self.conn.cursor()
        for elem in root:
            if db_version >= int(elem.attrib['id']):
                continue
            for subelem in elem:
                try:
                    cur.execute(subelem.text)
                    print('migration {}: {}'.format(elem.attrib['id'], subelem.attrib['id']))
                    self.save_settings(key='version', value=elem.attrib['id'])
                except sqlite3.Error as e:
                    self.logger.error("Database error: %s" % e)
                    break
                except Exception as e:
                    self.logger.error("Exception in _query: %s" % e)
                    break

    def db_version(self):
        if not self.table_exist('settings'):
            return 0
        try:
            cur = self.conn.cursor()
            cur.execute("SELECT value FROM settings WHERE key='version'")
            result = cur.fetchone()
            return result[0] if result else 0
        except Error as e:
            print(e)

    def table_exist(self, table_name):
        sql = ''' SELECT count(name) 
                FROM sqlite_master 
                WHERE type='table' AND name=? '''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (table_name, ))
            return cur.fetchone()[0] == 1
        except Exception as ex:
            self.logger.error(ex)

    # ============== TEACHER FUNCTIONS ==============
    def get_exam_time(self, exam_id):
        """
        Query all exam info
        :return date and duration of exam
        """
        sql = """
            SELECT e.id, e.name, strftime("%d-%m-%Y %H:%M", e.beg_date) [time],
            Cast ((JulianDay(e.end_date) - JulianDay(e.beg_date)) * 24 * 60 As Integer) [duration]
            FROM exam e
            WHERE e.id=?            
            """
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id, ))
            return cur.fetchone()
        except Exception as ex:
            self.logger.error(ex)

    def get_test_analyze(self, exam_id, chat_id):
        """
        Query all question and user answer related to current theme
        :param exam_id - exam
        :param chat_id - user chat_id
        :return list of user test results
        """
        sql = """
            SELECT strftime("%H:%M:%S", r.created) [began], 
            Cast ((JulianDay(r.finished) - JulianDay(r.created)) * 24 * 60 * 60 As Integer) [duration], t.question, 
            CASE WHEN r.user_answer = 1 THEN answer1 WHEN r.user_answer = 2 THEN answer2 WHEN r.user_answer = 3 THEN 
              answer3 WHEN r.user_answer = 4 THEN answer4 WHEN r.user_answer = 5 THEN answer5 END [user_answer], 
            CASE WHEN t.right_answer = 1 THEN answer1 WHEN t.right_answer = 2 THEN answer2 WHEN t.right_answer = 3 
              THEN answer3 WHEN t.right_answer = 4 THEN answer4 WHEN t.right_answer = 5 THEN answer5 END [right_answer], 
            t.right_answer == r.user_answer [is_true]			
            FROM test t
            LEFT JOIN test_results r ON t.id=r.test_id     
            WHERE t.test_group_id = (SELECT test_group_id from exam WHERE id=?) and (r.learner_id is null or r.learner_id=?)						
            ORDER BY r.created

            """
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id, chat_id, ))
            return cur.fetchall()
        except Exception as ex:
            print(ex)


    def learner_exam_list(self, chat_id):
        """
        Query all exam list related to learner
        :return list of exam
        """
        sql = """
            SELECT id, name, strftime("%d-%m-%Y", created) [date]
            FROM exam
            WHERE id in (SELECT DISTINCT exam_id FROM test_results WHERE learner_id=?)        
            """
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (chat_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)

    def get_exam_list(self, chat_id):
        """
        Query all exam list related to teacher
        :return list of exam
        """
        sql = """
            SELECT t.id, t.name, strftime("%d-%m-%Y", t.created) [date], t.test_group_id
            FROM exam t            
            WHERE  t.teacher_chat_id=?              
            """
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (chat_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)

    def get_class_id(self, name):
        sql = """
        SELECT id,class_type_id FROM pupil_class WHERE name=?"""
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (name.strip(),))
            row = cur.fetchone()
            return row if row else None
        except Exception as ex:
            self.logger.error(ex)

    def save_test(self, test_group_id, question, answers, right_answer):
        """
        Query saves newly created test
        :param question - question of test
        :param theme_id - theme_id of test.
        :param right_answer - right_answer of test
        :param answers - answers of test
        :return True if success otherwise False
        """
        try:
            cur = self.conn.cursor()
            args = (
                test_group_id,
                question,
                answers[0],
                answers[1],
                answers[2] if len(answers) > 2 else None,
                answers[3] if len(answers) > 3 else None,
                answers[4] if len(answers) > 4 else None,
                right_answer
            )
            sql = '''REPLACE INTO test(test_group_id, question, answer1,answer2,answer3,answer4,answer5, right_answer, test_type_id)
                                VALUES(?,?,?,?,?,?,?,?,1)'''
            cur.execute(sql, args)
            self.conn.commit()
            return True
        except Exception as ex:
            self.logger.error(ex)
            return False


    def get_pupil_class_by_exam(self, theme_id):
        sql = """
            SELECT p.id,p.name
            FROM pupil_class p
            LEFT JOIN timetable t on t.pupil_class_id=p.id
            WHERE t.theme_id=?
            """
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (theme_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)

    def get_test_number(self, exam_id):
        sql = "SELECT count(*) [total] from test where test_group_id=(SELECT test_group_id from exam where id=?)"
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id,))
            row = cur.fetchone()
            return row[0] if row else 0
        except Exception as ex:
            self.logger.error(ex)

    def get_student_list(self, exam_id):
        """
        Query all students by exam
        :param exam_id
        :return list of students
        """
        sql = """
            SELECT DISTINCT t.learner_id [id], l.first_name || " " || l.last_name [name]
            FROM test_results t
            LEFT JOIN learner l on l.chat_id = t.learner_id
            WHERE t.exam_id = ?                    
        """
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)

    def get_marks(self, exam_id):
        """
        Query all test result by theme and class
        :param exam_id
        :return list of marsk of pupil by class and theme
        """
        sql = """
            SELECT p.chat_id, p.last_name, p.first_name, 
            sum(user_answer == right_answer) [right], 
            count(user_answer) [answers]
            FROM learner p
            LEFT JOIN test_results r ON r.learner_id = p.chat_id
            LEFT JOIN test t ON r.test_id = t.id
            WHERE r.exam_id=?
            GROUP BY p.chat_id
            ORDER BY right DESC                    
        """
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (exam_id,))
            return cur.fetchall()
        except Exception as ex:
            self.logger.error(ex)